import React from "react";
import { Box } from "@chakra-ui/react";
import { Route, Switch } from "react-router-dom";
import Navbar from "./components/Navbar/Navbar";
import Signin from "./views/Auth/Signin";
import Home from "./views/Home";
import Reservas from "./views/Reservas";
import ReservaDetalle from "./views/ReservaDetalle";
import CheckIn from "./views/CheckIn";
import ForgotPassword from "./views/Auth/ForgotPassword";
import ForgotPasswordSubmit from "./views/Auth/ForgotPasswordSubmit";

import Signup from "./views/Auth/Signup";
import ConfirmSignup from "./views/Auth/ConfirmSignup";
import { useAuth } from "./context/AuthContext";

import { PrivateRouteAdmin, PrivateRouteClient, PublicRoute } from "./Routes";
import Footer from "./components/Footer/Footer";
import CheckInAdmin from "./views/Admin/CheckInAdmin";

function App() {
   let { initializing } = useAuth();
   
   if (initializing) {
      return null;
   }

   return (
      <>
         <Navbar />
         <Switch>
            <PublicRoute exact path="/signin">
               <Signin />
            </PublicRoute>
            <PublicRoute exact path="/signup">
               <Signup />
            </PublicRoute>
            <PublicRoute exact path="/confirm-signup">
               <ConfirmSignup />
            </PublicRoute>

            <Route exact path="/forgot-password">
               <ForgotPassword />
            </Route>
            <Route exact path="/forgot-submit">
               <ForgotPasswordSubmit />
            </Route>
            <Box pt="4rem">
               <Route exact path="/">
                  <Home />
               </Route>
               <PrivateRouteClient exact path="/reservas">
                  <Reservas />
               </PrivateRouteClient>
               <PrivateRouteClient exact path="/checkin/:id/:pax">
                  <CheckIn />
               </PrivateRouteClient>
               <PrivateRouteClient exact path="/reserva-detalle/:id">
                  <ReservaDetalle />
               </PrivateRouteClient>
               <PrivateRouteAdmin exact path="/handle-checkin">
                  <CheckInAdmin />
               </PrivateRouteAdmin>
            </Box>
         </Switch>
         <Footer />
      </>
   );
}

export default App;
