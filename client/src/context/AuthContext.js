import React, { createContext, useContext, useState, useEffect } from "react";
import { Auth, API, graphqlOperation } from "aws-amplify";
import { useHistory } from "react-router-dom";
import { createUsuario } from "../graphql/mutations";
import { listUsuarios } from "../graphql/queries";
import { useToast } from "@chakra-ui/react";
const AuthContext = createContext();

function useAuth() {
  return useContext(AuthContext);
}

function AuthProvider({ children }) {
  let [user, setUser] = useState(null);
  let [loading, setLoading] = useState(false);
  let [initializing, setInitializing] = useState(true);
  const history = useHistory();
  const toast = useToast();

  async function getUserByEmail(email) {
    try {
      let { data } = await API.graphql(
        graphqlOperation(listUsuarios, { filter: { email: { eq: email } } })
      );
      return data;
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al crear cuenta, ${error?.message}`,
        status: "error",
        duration: 2500,
        isClosable: true,
      });
    }
  }

  async function signUp(formData) {
    try {
      setLoading(true);
      let { user } = await Auth.signUp({
        username: formData.username,
        password: formData.password,
      });

      history.push("/confirm-signup");
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al crear cuenta, ${error?.message}`,
        status: "error",
        duration: 2500,
        isClosable: true,
      });
    } finally {
      setLoading(false);
    }
  }

  async function confirmSignUp(formData) {
    try {
      setLoading(true);
      await Auth.confirmSignUp(formData.username, formData.codeNumber);

      // let date = new Date();
      // date.setHours(date.getHours() - 5);
      // let dateUTC = new Date(date.toUTCString().slice(0, -3));
      const userData = {
        email: formData.username,
        type: formData.userType,
      };
      await API.graphql(graphqlOperation(createUsuario, { input: userData }));

      history.push("/signin");
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al crear cuenta, ${error?.message}`,
        status: "error",
        duration: 2500,
        isClosable: true,
      });
    } finally {
      setLoading(false);
    }
  }

  async function forgotPassword(formData) {
    try {
      setLoading(true);
      await Auth.forgotPassword(formData.username);
      history.push("/forgot-submit");
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al recuperar contraseña, ${error?.message}`,
        status: "error",
        duration: 2500,
        isClosable: true,
      });
    } finally {
      setLoading(false);
    }
  }

  async function forgotPasswordSubmit(formData) {
    try {
      setLoading(true);
      await Auth.forgotPasswordSubmit(
        formData.username,
        formData.codeNumber,
        formData.password
      );
      history.push("/signin");
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al restablecer contraseña, ${error?.message}`,
        status: "error",
        duration: 2500,
        isClosable: true,
      });
    } finally {
      setLoading(false);
    }
  }

  async function signIn(formData) {
    try {
      setLoading(true);
      let user = await Auth.signIn(formData.username, formData.password);
      let helper = await getUserByEmail(user?.attributes?.email);
      if (user) {
        if (helper?.listUsuarios?.items[0]?.type) {
          if (helper?.listUsuarios?.items[0]?.type === "Client") {
            user["userType"] = "CL";
          } else {
            user["userType"] = "AD";
          }
        }
      }
      setUser(user);
      history.push("/");
    } catch (error) {
      console.log(error);
      toast({
        position: "top-right",
        title: `Error al iniciar sesión, ${error?.message}`,
        status: "error",
        duration: 2500,
        isClosable: true,
      });
    } finally {
      setLoading(false);
    }
  }

  async function signOut() {
    try {
      setLoading(true);
      await Auth.signOut();
      setUser(null);
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al cerrar sesión, ${error?.message}`,
        status: "error",
        duration: 2500,
        isClosable: true,
      });
    } finally {
      setLoading(false);
    }
  }

  let value = {
    user,
    loading,
    initializing,
    signUp,
    confirmSignUp,
    forgotPassword,
    forgotPasswordSubmit,
    signIn,
    signOut,
  };

  async function initialize() {
    try {
      let user = await Auth.currentAuthenticatedUser();
      let helper = await getUserByEmail(user?.attributes?.email);
      if (user) {
        if (helper?.listUsuarios?.items[0]?.type) {
          if (helper?.listUsuarios?.items[0]?.type === "Client") {
            user["userType"] = "CL";
          } else {
            user["userType"] = "AD";
          }
        }
      }
      setUser(user);
    } catch (error) {
      console.log(error);
    } finally {
      setInitializing(false);
    }
  }

  useEffect(function () {
    initialize();
  }, []);

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export { useAuth, AuthProvider };
