import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { BrowserRouter as Router } from "react-router-dom";
import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import { AuthProvider } from "./context/AuthContext";
import Amplify from "aws-amplify";
import awsconfig from "./aws-exports";

Amplify.configure(awsconfig);

const theme = extendTheme({
   fonts: {
      heading: "'Lato', sans-serif",
      button: "'Montserrat', sans-serif",
      body: "'Montserrat', sans-serif",
      mono: "'Montserrat', sans-serif",
      footer: "'Montserrat', sans-serif",
   },
});

ReactDOM.render(
   <Router>
      <ChakraProvider theme={theme}>
         <AuthProvider>
            <App />
         </AuthProvider>
      </ChakraProvider>
   </Router>,
   document.getElementById("root")
);
