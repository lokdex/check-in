import React from "react";
import { useAuth } from "./context/AuthContext";
import { Route, Redirect } from "react-router-dom";

const PrivateRouteClient = ({ children, ...rest }) => {
   const { user } = useAuth();

   return (
      <Route
         {...rest}
         render={() => (!!user && user?.userType === "CL" ? children : <Redirect to="/signin" />)}
      />
   );
};

const PrivateRouteAdmin = ({ children, ...rest }) => {
   const { user } = useAuth();

   return (
      <Route
         {...rest}
         render={() => (!!user && user?.userType === "AD" ? children : <Redirect to="/signin" />)}
      />
   );
};

const PublicRoute = ({ children, ...rest }) => {
   const { user } = useAuth();
   return <Route {...rest} render={() => (!!user ? <Redirect to="/" /> : children)} />;
};

export { PrivateRouteClient, PrivateRouteAdmin, PublicRoute };
