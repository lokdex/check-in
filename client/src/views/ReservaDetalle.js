import React, { useEffect, useState } from "react";
import { Divider, Flex, Heading, SimpleGrid, Text } from "@chakra-ui/react";
import { API, graphqlOperation } from "aws-amplify";
import { getReserva } from "../graphql/queries";
import { useParams } from "react-router";
import Spinner from "../components/Spinner/Spinner";
const ReservaDetalle = () => {
   let { id } = useParams();
   let [reserva, setReserva] = useState(null);

   async function fetchData() {
      let { data } = await API.graphql(graphqlOperation(getReserva, { id: id }));
      setReserva(data?.getReserva);
   }

   useEffect(() => {
      fetchData();
   }, []);

   return (
      <Flex
         minHeight="100vh"
         width="full"
         align="center"
         justifyContent="center"
         flexDirection="row"
         p={{ base: 8, sm: 8, md: 16, lg: 16, xl: 16 }}
         textAlign="center"
      >
         {!!reserva ? (
            <Flex
               w="container.lg"
               direction="column"
               borderRadius={8}
               p={8}
               boxShadow=" 0 10px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)"
            >
               <Heading mt={8} mb={4} fontSize="32px">
                  Detalle Reserva
               </Heading>
               <Divider mb={8} />
               <SimpleGrid columns={2} spacing={8}>
                  <Heading fontSize="18px" lineHeight="19px">
                     Nro. doc. cliente
                  </Heading>

                  <Text>{reserva?.cliente?.docNumber}</Text>
                  <Heading fontSize="18px" lineHeight="19px">
                     Nombre del cliente
                  </Heading>

                  <Text>
                     {reserva?.cliente?.givenName} {reserva?.cliente?.lastName}
                  </Text>
                  <Heading fontSize="18px" lineHeight="19px">
                     Correo del cliente
                  </Heading>

                  <Text>{reserva?.clienteMail}</Text>

                  <Heading fontSize="18px" lineHeight="19px">
                     Número de noches
                  </Heading>

                  <Text>{reserva?.nightQty}</Text>

                  <Heading fontSize="18px" lineHeight="19px">
                     Número de habitaciones
                  </Heading>

                  <Text>{reserva?.roomQty}</Text>

                  <Heading fontSize="18px" lineHeight="19px">
                     Fecha In
                  </Heading>

                  <Text>{reserva?.dateIn}</Text>

                  <Heading fontSize="18px" lineHeight="19px">
                     Fecha Out
                  </Heading>

                  <Text>{reserva?.dateOut}</Text>

                  <Heading fontSize="18px" lineHeight="19px">
                     Estado de la reserva
                  </Heading>

                  <Text>
                     {reserva?.status === "T"
                        ? "Tentativa"
                        : reserva?.status === "R"
                        ? "Confirmada"
                        : reserva?.status === "A"
                        ? "Anulada"
                        : reserva?.status === "CI"
                        ? "Check In "
                        : reserva?.status === "W"
                        ? "Web Check In"
                        : reserva?.status === "CO"
                        ? "Check Out"
                        : "Estado incorrecto"}
                  </Text>

                  <Heading fontSize="18px" lineHeight="19px">
                     Tarifa neta total
                  </Heading>

                  <Text>S/. {reserva?.tarifaTotal}</Text>
                  <Heading fontSize="18px" lineHeight="19px">
                     Método de pago
                  </Heading>

                  <Text>{reserva?.paymentMethod}</Text>

                  {reserva?.adultPAX > 0 && (
                     <>
                        <Heading fontSize="18px" lineHeight="19px">
                           Cantidad de pasajeros adultos
                        </Heading>

                        <Text>{reserva?.adultPAX}</Text>
                     </>
                  )}
                  {reserva?.kidPAX > 0 && (
                     <>
                        <Heading fontSize="18px" lineHeight="19px">
                           Cantidad de pasajeros niños
                        </Heading>

                        <Text>{reserva?.kidPAX}</Text>
                     </>
                  )}
                  {reserva?.babyPAX > 0 && (
                     <>
                        <Heading fontSize="18px" lineHeight="19px">
                           Cantidad de pasajeros bebés
                        </Heading>

                        <Text>{reserva?.babyPAX}</Text>
                     </>
                  )}

                  <Heading fontSize="18px" lineHeight="19px">
                     Total de pasajeros
                  </Heading>

                  <Text>{reserva?.totalPAX}</Text>
               </SimpleGrid>
            </Flex>
         ) : (
            <Spinner />
         )}
      </Flex>
   );
};

export default ReservaDetalle;
