import React, { useEffect, useState } from "react";
import { API, graphqlOperation, Storage } from "aws-amplify";
import { useHistory } from "react-router";
import { listReservas, getReserva } from "../../graphql/queries";
import { updateReserva } from "../../graphql/mutations";
import { useAuth } from "../../context/AuthContext";
import moment from "moment";
import MomentUtils from "@date-io/moment";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import Spinner from "../../components/Spinner/Spinner";
import { MdEdit } from "react-icons/md";
import "./CheckInAdmin.css";

//Components
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  IconButton,
  Input,
  InputGroup,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
  Image,
  Select,
  Table,
  Td,
  Textarea,
  Thead,
  Th,
  Tr,
  Tbody,
  Text,
  useDisclosure,
  SimpleGrid,
  GridItem,
  useToast,
  useMediaQuery,
} from "@chakra-ui/react";

const CheckInAdmin = () => {
  let { user } = useAuth();
  const history = useHistory();
  const [reservas, setReservas] = useState([]);

  const [loading, setLoading] = useState(false);
  let [reserva, setReserva] = useState();
  //Data para el formulario
  const [id, setId] = useState(-1);
  const [id2, setId2] = useState(-1);
  const [isMobile] = useMediaQuery("(min-width: 480px)");
  const [dateIn, setDateIn] = useState(new Date());
  const [dateOut, setDateOut] = useState(new Date());
  const [huesData, setHuesData] = useState([]);
  const [docsHuespedes, setDocsHuespedes] = useState([]);

  //Toast
  const [onHover, setHover] = useState(false);
  const toast = useToast();
  //Modales
  // Loading
  const { isOpen, onOpen, onClose } = useDisclosure();
  let size;
  !isMobile ? (size = "xs") : (size = "4xl");

  //Editar
  const {
    isOpen: isOpenEditModal,
    onOpen: onOpenEditModal,
    onClose: onCloseEditModal,
  } = useDisclosure();

  //Helpers
  const editReservaModal = (idx) => {
    setId(idx);
    fetchReserva(idx);
    onOpenEditModal();
  };

  const onCloseEdit = () => {
    setDocsHuespedes(null);
    setId(-1);
    setDateIn(new Date());
    setDateOut(new Date());
    onCloseEditModal();
  };

  //Fetch Data

  async function fetchReservas() {
    try {
      let { data } = await API.graphql(graphqlOperation(listReservas));

      let reser = data?.listReservas?.items;

      reser.sort(function (a, b) {
        return moment(a.dateIn) - moment(b.dateIn);
      });

      setReservas(reser);
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al recuperar reservas, ${error?.message}`,
        status: "error",
        duration: 2500,
        isClosable: true,
      });
    }
  }

  async function fetchReserva(id) {
    try {
      let { data } = await API.graphql(
        graphqlOperation(getReserva, { id: id })
      );

      setReserva(data?.getReserva);
      setDateIn(
        moment(data?.getReserva?.dateIn, "DD/MM/YYYY").format("MM/DD/YYYY")
      );
      setDateOut(
        moment(data?.getReserva?.dateOut, "DD/MM/YYYY").format("MM/DD/YYYY")
      );

      console.log(
        moment(data?.getReserva?.dateOut, "DD/MM/YYYY").format("MM/DD/YYYY")
      );
      let huesData = data?.getReserva?.huespedes?.items;
      setHuesData(huesData);

      let docsH = [];
      let urlDH;
      let urlsDH = [];
      data?.getReserva?.huespedes?.items.map((item) =>
        docsH.push(item?.docImg)
      );
      for (let i = 0; i < docsH.length; i++) {
        urlDH = await Storage.get(docsH[i]);
        urlsDH.push(urlDH);
      }
      setDocsHuespedes(urlsDH);
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al recuperar detalle de reserva, ${error?.message}`,
        status: "error",
        duration: 2500,
        isClosable: true,
      });
    }
  }

  const submitEditReserva = async (e) => {
    e.preventDefault();

    let formData = new FormData(e.target);

    let paxtot = reserva?.totalPAX;
    let paxadu = parseInt(formData.get("ADUPAX"));
    let paxbab = parseInt(formData.get("BABYPAX"));
    let paxkid = parseInt(formData.get("KIDPAX"));

    if (paxadu + paxbab + paxkid !== paxtot) {
      toast({
        position: "top-right",
        title: "El número de pasajeros no coincide con el total registrado",
        status: "warning",
        duration: 2500,
        isClosable: true,
      });
      return;
    }

    let inputData = {
      id: reserva?.id,
      PAXinside: formData.get("PAXIN"),
      adultPAX: paxadu,
      babyPAX: paxbab,
      kidPAX: paxkid,
      comments: formData.get("COMENT") || "",
      dateIn: formData.get("FECIN"),
      dateOut: formData.get("FECSA"),
      nightQty: formData.get("N8QTY"),
      paymentMethod: formData.get("METPAG"),
      pensionType: formData.get("PENSION"),
      status: formData.get("ESTADO"),
      tarifaTotal: formData.get("TARTOT"),
      updatedBy: user?.attributes?.email,
    };

    try {
      setLoading(true);
      onOpen();
      await API.graphql(graphqlOperation(updateReserva, { input: inputData }));
      toast({
        position: "top-right",
        title: "Cambios guardados",
        status: "success",
        duration: 1500,
        isClosable: true,
      });
      onClose();
      history.push("/handle-checkin");
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al actualizar reserva, ${error?.message}`,
        status: "error",
        duration: 1500,
        isClosable: true,
      });
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchReservas();
  }, []);

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      {loading && (
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay>
            <ModalContent>
              <ModalBody>
                <Spinner />
              </ModalBody>
            </ModalContent>
          </ModalOverlay>
        </Modal>
      )}
      <Box minH="100vh" w="100%" p={{ base: 4, sm: 4, md: 8, lg: 8, xl: 8 }}>
        <Box p={{ base: 4, sm: 4, md: 8, lg: 8, xl: 8 }} w="100%">
          <Heading fontSize="24px">Gestionar Reservas</Heading>
        </Box>
        {!!reservas.length ? (
          <Box
            bg="white"
            p={{ base: 4, sm: 4, md: 8, lg: 8, xl: 8 }}
            border="1px solid"
            boxSizing="border-box"
            borderColor="gray.200"
            borderRadius="1rem"
            mt={8}
          >
            <Table
              variant="simple"
              size={{ base: "sm", sm: "sm", md: "md", lg: "md", xl: "lg" }}
              overflow="visible"
              w="100%"
              display="table"
            >
              <Thead>
                <Tr>
                  <Th
                    style={{
                      textAlign: "center",
                    }}
                    fontSize={{
                      base: "0.8rem",
                      sm: "0.8",
                      md: "0.875rem",
                      lg: "0.875rem",
                      xl: "0.875rem",
                    }}
                  >
                    Nombre Cliente
                  </Th>
                  <Th
                    style={{
                      textAlign: "center",
                    }}
                    fontSize={{
                      base: "0.8rem",
                      sm: "0.8",
                      md: "0.875rem",
                      lg: "0.875rem",
                      xl: "0.875rem",
                    }}
                  >
                    Correo Cliente
                  </Th>
                  <Th
                    style={{
                      textAlign: "center",
                    }}
                    fontSize={{
                      base: "0.8rem",
                      sm: "0.8",
                      md: "0.875rem",
                      lg: "0.875rem",
                      xl: "0.875rem",
                    }}
                    display={{
                      base: "none",
                      sm: "none",
                      md: "table-cell",
                      lg: "table-cell",
                    }}
                  >
                    Fecha de Ingreso
                  </Th>
                  <Th
                    style={{
                      textAlign: "center",
                    }}
                    fontSize={{
                      base: "0.8rem",
                      sm: "0.8",
                      md: "0.875rem",
                      lg: "0.875rem",
                      xl: "0.875rem",
                    }}
                    display={{
                      base: "none",
                      sm: "none",
                      md: "table-cell",
                      lg: "table-cell",
                    }}
                  >
                    Fecha de Salida
                  </Th>
                  <Th
                    style={{
                      textAlign: "center",
                    }}
                    fontSize={{
                      base: "0.8rem",
                      sm: "0.8",
                      md: "0.875rem",
                      lg: "0.875rem",
                      xl: "0.875rem",
                    }}
                    display={{
                      base: "none",
                      sm: "none",
                      md: "table-cell",
                      lg: "table-cell",
                    }}
                  >
                    Estado
                  </Th>

                  <Th
                    style={{
                      textAlign: "center",
                    }}
                    fontSize={{
                      base: "0.8rem",
                      sm: "0.8",
                      md: "0.875rem",
                      lg: "0.875rem",
                      xl: "0.875rem",
                    }}
                  >
                    Editar
                  </Th>
                </Tr>
              </Thead>
              <Tbody style={{ display: "table-row-group" }}>
                {reservas.map((reserva, idx) => (
                  <Tr verticalAlign="middle" key={reserva?.id}>
                    <Td
                      style={{
                        textAlign: "center",
                      }}
                      fontSize={{
                        base: "0.7rem",
                        sm: "0.7rem",
                        md: "0.875rem",
                        lg: "0.875rem",
                        xl: "0.875rem",
                      }}
                      maxW="16rem"
                    >
                      {reserva?.cliente?.givenName +
                        " " +
                        reserva?.cliente?.lastName || (
                        <Text color="gray" textAlign="center">
                          Cliente no disponible
                        </Text>
                      )}
                    </Td>

                    <Td
                      style={{
                        textAlign: "center",
                      }}
                      fontSize={{
                        base: "0.7rem",
                        sm: "0.7rem",
                        md: "0.875rem",
                        lg: "0.875rem",
                        xl: "0.875rem",
                      }}
                      maxW="16rem"
                    >
                      {reserva?.clienteMail || (
                        <Text color="gray" textAlign="center">
                          Correo no disponible
                        </Text>
                      )}
                    </Td>

                    <Td
                      style={{
                        textAlign: "center",
                      }}
                      minW="8rem"
                      maxW="16rem"
                      fontSize={{
                        base: "0.7rem",
                        sm: "0.7rem",
                        md: "0.875rem",
                        lg: "0.875rem",
                        xl: "0.875rem",
                      }}
                      display={{
                        base: "none",
                        sm: "none",
                        md: "table-cell",
                        lg: "table-cell",
                      }}
                    >
                      {reserva?.dateIn || (
                        <Text color="gray" textAlign="center">
                          Fecha no disponible
                        </Text>
                      )}
                    </Td>
                    <Td
                      style={{
                        textAlign: "center",
                      }}
                      minW="8rem"
                      maxW="16rem"
                      fontSize={{
                        base: "0.7rem",
                        sm: "0.7rem",
                        md: "0.875rem",
                        lg: "0.875rem",
                        xl: "0.875rem",
                      }}
                      display={{
                        base: "none",
                        sm: "none",
                        md: "table-cell",
                        lg: "table-cell",
                      }}
                    >
                      {reserva?.dateOut || (
                        <Text color="gray" textAlign="center">
                          Fecha no disponible
                        </Text>
                      )}
                    </Td>
                    <Td
                      style={{
                        textAlign: "center",
                      }}
                      minW="8rem"
                      maxW="16rem"
                      fontSize={{
                        base: "0.7rem",
                        sm: "0.7rem",
                        md: "0.875rem",
                        lg: "0.875rem",
                        xl: "0.875rem",
                      }}
                      display={{
                        base: "none",
                        sm: "none",
                        md: "table-cell",
                        lg: "table-cell",
                      }}
                    >
                      {reserva?.status === "T"
                        ? "Tentativa"
                        : reserva?.status === "R"
                        ? "Confirmada"
                        : reserva?.status === "A"
                        ? "Anulada"
                        : reserva?.status === "CI"
                        ? "Check In "
                        : reserva?.status === "W"
                        ? "Web Check In"
                        : reserva?.status === "CO"
                        ? "Check Out"
                        : "Estado incorrecto"}
                    </Td>

                    <Td
                      style={{
                        textAlign: "center",
                      }}
                    >
                      <Flex justifyContent="center">
                        <IconButton
                          aria-label="edit reserva"
                          size="sm"
                          style={{
                            backgroundColor: "#2BD9A3",
                          }}
                          borderRadius="4"
                          ml={4}
                          icon={<MdEdit style={{ color: "white" }} />}
                          onClick={() => editReservaModal(reserva?.id)}
                        />
                      </Flex>
                    </Td>
                  </Tr>
                ))}
              </Tbody>
            </Table>
          </Box>
        ) : (
          <Spinner />
        )}
        {/**Modal Editar */}
        {id !== -1 &&
          !!reserva?.dateIn &&
          !!reserva?.dateOut &&
          !!docsHuespedes && (
            <Modal isOpen={isOpenEditModal} onClose={onCloseEdit} size={size}>
              <ModalOverlay />
              <form onSubmit={submitEditReserva}>
                <ModalContent>
                  <ModalHeader>Revisión de Reserva</ModalHeader>
                  <ModalCloseButton />
                  <ModalBody>
                    <SimpleGrid columns={2} spacing={10}>
                      <FormControl>
                        <FormLabel>Fecha de Ingreso</FormLabel>
                        <KeyboardDatePicker
                          value={dateIn}
                          onChange={(date) => setDateIn(date)}
                          autoOk
                          variant="inline"
                          inputVariant="outlined"
                          format="DD/MM/yyyy"
                          style={{ width: "100%" }}
                          name="FECIN"
                          disabled
                        />
                      </FormControl>
                      <FormControl>
                        <FormLabel>Fecha de Salida</FormLabel>
                        <KeyboardDatePicker
                          value={dateOut}
                          onChange={(date1) => setDateOut(date1)}
                          variant="inline"
                          inputVariant="outlined"
                          format="DD/MM/yyyy"
                          style={{ width: "100%" }}
                          name="FECSA"
                          disabled
                        />
                      </FormControl>
                      <FormControl>
                        <FormLabel>Método de Pago</FormLabel>
                        <Input
                          placeholder="Método de Pago"
                          name="METPAG"
                          defaultValue={reserva?.paymentMethod}
                          disabled
                        />
                      </FormControl>
                      <FormControl isRequired>
                        <FormLabel>Estado</FormLabel>
                        <InputGroup>
                          <Select defaultValue={reserva?.status} name="ESTADO">
                            <option value="T">Tentativa</option>
                            <option value="R">Reservada</option>
                            <option value="A">Anulada</option>
                            <option value="CI">Check In</option>
                            <option value="W">Web Check In</option>
                            <option value="CO">Check Out</option>
                          </Select>
                        </InputGroup>
                      </FormControl>
                      <FormControl>
                        <FormLabel>Número de Noches</FormLabel>
                        <NumberInput
                          disabled
                          step={1}
                          min={1}
                          defaultValue={reserva?.nightQty}
                        >
                          <NumberInputField
                            name="N8QTY"
                            onKeyDown={(event) => {
                              if (["e", "E", "+", "-"].includes(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                          <NumberInputStepper>
                            <NumberIncrementStepper />
                            <NumberDecrementStepper />
                          </NumberInputStepper>
                        </NumberInput>
                      </FormControl>
                      <FormControl>
                        <FormLabel>Número de Habitaciones</FormLabel>
                        <NumberInput
                          step={1}
                          min={1}
                          defaultValue={reserva?.roomQty}
                          disabled
                        >
                          <NumberInputField
                            name="ROMQTY"
                            onKeyDown={(event) => {
                              if (["e", "E", "+", "-"].includes(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                          <NumberInputStepper>
                            <NumberIncrementStepper />
                            <NumberDecrementStepper />
                          </NumberInputStepper>
                        </NumberInput>
                      </FormControl>
                      <FormControl>
                        <FormLabel>Tarifa Total</FormLabel>
                        <NumberInput
                          precision={2}
                          step={1}
                          min={1}
                          defaultValue={reserva?.tarifaTotal}
                          disabled
                        >
                          <NumberInputField
                            name="TARTOT"
                            onKeyDown={(event) => {
                              if (["e", "E", "+", "-"].includes(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                          <NumberInputStepper>
                            <NumberIncrementStepper />
                            <NumberDecrementStepper />
                          </NumberInputStepper>
                        </NumberInput>
                      </FormControl>
                      <FormControl>
                        <FormLabel>Total de Pasajeros</FormLabel>
                        <NumberInput
                          step={1}
                          min={1}
                          defaultValue={reserva?.totalPAX}
                          disabled
                        >
                          <NumberInputField
                            name="TOTPAX"
                            onKeyDown={(event) => {
                              if (["e", "E", "+", "-"].includes(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                          <NumberInputStepper>
                            <NumberIncrementStepper />
                            <NumberDecrementStepper />
                          </NumberInputStepper>
                        </NumberInput>
                      </FormControl>
                      <FormControl>
                        <FormLabel>Nro. Pasajeros adultos</FormLabel>
                        <NumberInput
                          step={1}
                          min={1}
                          defaultValue={reserva?.adultPAX}
                          disabled
                        >
                          <NumberInputField
                            name="ADUPAX"
                            onKeyDown={(event) => {
                              if (["e", "E", "+", "-"].includes(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                          <NumberInputStepper>
                            <NumberIncrementStepper />
                            <NumberDecrementStepper />
                          </NumberInputStepper>
                        </NumberInput>
                      </FormControl>
                      <FormControl>
                        <FormLabel>Nro. Pasajeros niños</FormLabel>
                        <NumberInput
                          step={1}
                          min={1}
                          defaultValue={reserva?.kidPAX}
                          disabled
                        >
                          <NumberInputField
                            name="KIDPAX"
                            onKeyDown={(event) => {
                              if (["e", "E", "+", "-"].includes(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                          <NumberInputStepper>
                            <NumberIncrementStepper />
                            <NumberDecrementStepper />
                          </NumberInputStepper>
                        </NumberInput>
                      </FormControl>
                      <FormControl>
                        <FormLabel>Nro. Pasajeros bebés</FormLabel>
                        <NumberInput
                          step={1}
                          defaultValue={reserva?.babyPAX}
                          disabled
                        >
                          <NumberInputField
                            name="BABYPAX"
                            onKeyDown={(event) => {
                              if (["e", "E", "+", "-"].includes(event.key)) {
                                event.preventDefault();
                              }
                            }}
                          />
                          <NumberInputStepper>
                            <NumberIncrementStepper />
                            <NumberDecrementStepper />
                          </NumberInputStepper>
                        </NumberInput>
                      </FormControl>
                      <FormControl>
                        <FormLabel>Huéspedes adentro</FormLabel>
                        <InputGroup>
                          <Select
                            defaultValue={reserva?.PAXinside}
                            name="PAXIN"
                            disabled
                          >
                            <option value={1}>Sí</option>
                            <option value={0}>No</option>
                          </Select>
                        </InputGroup>
                      </FormControl>
                      <FormControl>
                        <FormLabel>Tipo de pensión</FormLabel>
                        <InputGroup>
                          <Select
                            defaultValue={reserva?.pensionType}
                            name="PENSION"
                            disabled
                          >
                            <option value="BF">BF</option>
                            <option value="HB">HB</option>
                            <option value="FB">FB</option>
                          </Select>
                        </InputGroup>
                      </FormControl>
                      <GridItem colSpan={2}>
                        <FormControl>
                          <FormLabel>Comentarios adicionales</FormLabel>
                          <Textarea
                            placeholder="Notas sobre la reserva"
                            name="COMENT"
                            defaultValue={reserva?.comments}
                          />
                        </FormControl>
                      </GridItem>
                      <GridItem colSpan={2}>
                        <FormControl>
                          <FormLabel>Datos de los huéspedes</FormLabel>

                          {huesData.length ? (
                            huesData.map((item, idx) => (
                              <ul>
                                <span
                                  style={{
                                    fontWeight: "bold",
                                    fontSize: "1.5em",
                                  }}
                                >
                                  Huesped {idx + 1}
                                </span>
                                <li>
                                  <span style={{ fontWeight: "bold" }}>
                                    Nombre:{` `}
                                  </span>
                                  {item.givenName} {` `} {item.lastName}
                                </li>
                                <li>
                                  <span style={{ fontWeight: "bold" }}>
                                    Dirección:{` `}
                                  </span>
                                  {item.address}
                                </li>
                                <li>
                                  <span style={{ fontWeight: "bold" }}>
                                    Tipo de documento:{` `}
                                  </span>
                                  {item.docType}
                                </li>
                                <li>
                                  <span style={{ fontWeight: "bold" }}>
                                    Número de documento:{` `}
                                  </span>
                                  {item.docNumber}
                                </li>
                                <li>
                                  <span style={{ fontWeight: "bold" }}>
                                    Fecha de nacimiento:{` `}
                                  </span>
                                  {item.birthdate}
                                </li>
                                <li>
                                  <span style={{ fontWeight: "bold" }}>
                                    Celular:{` `}
                                  </span>
                                  {item.cellphone}
                                </li>
                                <li>
                                  <span style={{ fontWeight: "bold" }}>
                                    Teléfono:{` `}
                                  </span>
                                  {item.phone}
                                </li>
                                <li>
                                  <span style={{ fontWeight: "bold" }}>
                                    Género:{` `}
                                  </span>
                                  {item.gender}
                                </li>
                                <li>
                                  <span style={{ fontWeight: "bold" }}>
                                    Nacionalidad:{` `}
                                  </span>
                                  {item.nationality}
                                </li>
                                <li>
                                  <span style={{ fontWeight: "bold" }}>
                                    Email:{` `}
                                  </span>
                                  {item.email}
                                </li>
                              </ul>
                            ))
                          ) : (
                            <Box>No existen huespedes</Box>
                          )}
                        </FormControl>
                      </GridItem>
                      <GridItem colSpan={2}>
                        <FormControl>
                          <FormLabel>Documentos de los huéspedes</FormLabel>

                          {docsHuespedes.length ? (
                            docsHuespedes.map((item, idx) => (
                              <Image src={item} key={`img-${idx}`} m="auto" />
                            ))
                          ) : (
                            <Box>No existen huespedes</Box>
                          )}
                        </FormControl>
                      </GridItem>
                    </SimpleGrid>
                  </ModalBody>

                  <ModalFooter>
                    <Button
                      type="submit"
                      onMouseEnter={() => setHover(true)}
                      onMouseLeave={() => setHover(false)}
                      style={{
                        backgroundColor: `${onHover ? "#00a3c4" : "#2BD9A3"}`,
                      }}
                      variant="solid"
                      color="white"
                      mr={4}
                    >
                      Guardar
                    </Button>

                    <Button
                      type="button"
                      colorScheme="cyan"
                      variant="outline"
                      onClick={onCloseEdit}
                    >
                      Cancelar
                    </Button>
                  </ModalFooter>
                </ModalContent>
              </form>
            </Modal>
          )}
      </Box>
    </MuiPickersUtilsProvider>
  );
};

export default CheckInAdmin;
