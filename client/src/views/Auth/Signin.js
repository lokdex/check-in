import React, { useState, useRef } from "react";
//Components
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Heading,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
  useToast,
  useDisclosure,
} from "@chakra-ui/react";
import Spinner from "../../components/Spinner/Spinner";

import { Link } from "react-router-dom";
import { MdEmail, MdLockOutline } from "react-icons/md";
import { IoMdEye, IoMdEyeOff } from "react-icons/io";

// AWS Amplify
import { useAuth } from "../../context/AuthContext";

const Signin = () => {
  let { signIn } = useAuth();
  //UI
  const [show, setShow] = useState(true);
  const [loading, setLoading] = useState(false);
  const [onHover, setHover] = useState(false);
  const [onHover1, setHover1] = useState(false);
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();

  //User Data
  let username = useRef();
  let password = useRef();

  async function handleSubmit(e) {
    e.preventDefault();
    try {
      setLoading(true);
      onOpen();
      let formData = {
        username: username.current.value,
        password: password.current.value,
      };
      await signIn(formData);
      onClose();
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al iniciar sesión, ${error?.message}`,
        status: "error",
        duration: 1500,
        isClosable: true,
      });
    } finally {
      setLoading(false);
    }
  }

  return (
    <Flex
      minHeight="100vh"
      width="full"
      align="center"
      justifyContent="center"
      flexDirection="row"
    >
      {loading && (
        <Modal isOpen={isOpen} onClose={onClose} isCentered>
          <ModalOverlay>
            <ModalContent>
              <ModalBody>
                <Spinner />
              </ModalBody>
            </ModalContent>
          </ModalOverlay>
        </Modal>
      )}
      <Box
        m={4}
        direction="row"
        borderRadius={8}
        minH="30rem"
        boxShadow=" 0 10px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)"
      >
        <Box p={8} m="auto">
          <Heading textAlign="center" fontSize="32px" m={8}>
            Check In
          </Heading>
          <Box textAlign="left">
            <form onSubmit={handleSubmit}>
              <FormControl mt={8} isRequired>
                <FormLabel>Email</FormLabel>
                <InputGroup>
                  <InputLeftElement
                    pointerEvents="none"
                    children={
                      <MdEmail
                        color="gray"
                        size="1.5rem"
                        style={{ marginTop: "0.1rem" }}
                      />
                    }
                  />
                  <Input
                    type="text"
                    placeholder="email"
                    size="md"
                    style={{
                      boxShadow:
                        " 0 4px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)",
                    }}
                    ref={username}
                  />
                </InputGroup>
              </FormControl>
              <FormControl isRequired mt={8}>
                <FormLabel>Password</FormLabel>
                <InputGroup>
                  <InputLeftElement
                    pointerEvents="none"
                    children={
                      <MdLockOutline
                        size="1.5rem"
                        color="gray"
                        style={{ marginTop: "0.1rem" }}
                      />
                    }
                  />
                  <Input
                    type={show ? "password" : "text"}
                    placeholder="*******"
                    size="md"
                    style={{
                      boxShadow:
                        " 0 4px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)",
                    }}
                    ref={password}
                  />
                  <InputRightElement w="3.5rem">
                    <Button h="1.75rem" p="0" onClick={() => setShow(!show)}>
                      {show ? (
                        <IoMdEyeOff size="1.5rem" color="gray" />
                      ) : (
                        <IoMdEye size="1.5rem" color="gray" />
                      )}
                    </Button>
                  </InputRightElement>
                </InputGroup>
              </FormControl>
              <Button
                type="submit"
                width="full"
                onMouseEnter={() => setHover(true)}
                onMouseLeave={() => setHover(false)}
                style={{
                  backgroundColor: `${onHover ? "#00a3c4" : "#2BD9A3"}`,
                }}
                variant="solid"
                color="white"
                mt={8}
                size="sm"
              >
                Iniciar Sesión
              </Button>

              <Link to="/signup">
                <Button
                  type="button"
                  colorScheme="cyan"
                  variant="outline"
                  width="full"
                  mt={4}
                  size="sm"
                >
                  Registrarse
                </Button>
              </Link>
              <Box mt={8} textAlign="center">
                <Link
                  to="/forgot-password"
                  onMouseEnter={() => setHover1(true)}
                  onMouseLeave={() => setHover1(false)}
                  style={{
                    color: `${onHover1 ? "#2BD9A3" : "black"}`,
                    textDecoration: `${onHover1 ? "underline" : "none"}`,
                  }}
                >
                  Olvidé mi contraseña
                </Link>
              </Box>
            </form>
          </Box>
        </Box>
      </Box>
    </Flex>
  );
};

export default Signin;
