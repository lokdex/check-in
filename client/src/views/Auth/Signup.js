import React, { useState, useRef } from "react";
//Components
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Heading,
  useToast,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { MdEmail, MdLockOutline } from "react-icons/md";
import { IoMdEye, IoMdEyeOff } from "react-icons/io";

// AWS Amplify
import { useAuth } from "../../context/AuthContext";

const Signup = () => {
  //Auth
  let { signUp } = useAuth();
  //UI state
  const [show, setShow] = useState(true);
  const [onHover1, setHover1] = useState(false);
  const toast = useToast();
  //User Data
  let username = useRef();
  let password = useRef();

  function handleSubmit(e) {
    e.preventDefault();
    try {
      let formData = {
        username: username.current.value,
        password: password.current.value,
      };
      signUp(formData);
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al crear una cuenta, ${error?.message}`,
        status: "error",
        duration: 1500,
        isClosable: true,
      });
    }
  }

  return (
    <Flex
      minHeight="100vh"
      width="full"
      align="center"
      justifyContent="center"
      flexDirection="row"
    >
      <Box
        m={4}
        direction="row"
        borderRadius={8}
        boxShadow=" 0 10px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)"
      >
        <Box p={8} m="auto">
          <Heading textAlign="center" fontSize="24px">
            CREAR UNA CUENTA
          </Heading>
          <Box textAlign="left">
            <form onSubmit={handleSubmit}>
              <FormControl mt={8} isRequired>
                <FormLabel>Email</FormLabel>
                <InputGroup w="24rem">
                  <InputLeftElement
                    pointerEvents="none"
                    children={
                      <MdEmail
                        color="gray"
                        size="1.5rem"
                        style={{ marginTop: "0.1rem" }}
                      />
                    }
                  />
                  <Input
                    type="text"
                    placeholder="email"
                    size="md"
                    style={{
                      boxShadow:
                        " 0 4px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)",
                    }}
                    ref={username}
                  />
                </InputGroup>
              </FormControl>
              <FormControl isRequired mt={8}>
                <FormLabel>Password</FormLabel>
                <InputGroup w="24rem">
                  <InputLeftElement
                    pointerEvents="none"
                    children={
                      <MdLockOutline
                        size="1.5rem"
                        color="gray"
                        style={{ marginTop: "0.1rem" }}
                      />
                    }
                  />
                  <Input
                    type={show ? "password" : "text"}
                    placeholder="*******"
                    size="md"
                    style={{
                      boxShadow:
                        " 0 4px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)",
                    }}
                    ref={password}
                  />
                  <InputRightElement w="3.5rem">
                    <Button h="1.75rem" p="0" onClick={() => setShow(!show)}>
                      {show ? (
                        <IoMdEyeOff size="1.5rem" color="gray" />
                      ) : (
                        <IoMdEye size="1.5rem" color="gray" />
                      )}
                    </Button>
                  </InputRightElement>
                </InputGroup>
              </FormControl>

              <Box
                mt={8}
                display="flex"
                flexDirection="column"
                textAlign="center"
              >
                <Button
                  type="submit"
                  style={{ backgroundColor: "#2BD9A3" }}
                  variant="solid"
                  color="white"
                  w="16rem"
                  m="auto"
                  size="sm"
                >
                  REGISTRARSE
                </Button>
                <br />

                <Link
                  to="/signin"
                  onMouseEnter={() => setHover1(true)}
                  onMouseLeave={() => setHover1(false)}
                  style={{
                    fontSize: "0.9rem",
                    color: `${onHover1 ? "#2BD9A3" : "black"}`,
                    textDecoration: `${onHover1 ? "underline" : "none"}`,
                    marginTop: "0.8rem",
                  }}
                >
                  ¿Ya tiene una cuenta?
                </Link>
              </Box>
            </form>
          </Box>
        </Box>
      </Box>
    </Flex>
  );
};

export default Signup;
