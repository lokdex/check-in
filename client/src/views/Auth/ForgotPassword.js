import React, { useState, useRef } from "react";

// AWS Amplify
import { useAuth } from "../../context/AuthContext";

//Components
import { Link } from "react-router-dom";
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  InputGroup,
  InputLeftElement,
  useToast,
} from "@chakra-ui/react";

//Media
import { MdEmail } from "react-icons/md";

const ForgotPassword = () => {
  //UI state
  const [onHover, setHover] = useState(false);
  const [onHover1, setHover1] = useState(false);

  //Autenticación
  const emailRef = useRef();
  const { forgotPassword } = useAuth();
  const [loading, setLoading] = useState(false);
  const toast = useToast();

  function handleSubmit(e) {
    e.preventDefault();

    try {
      let formData = {
        username: emailRef.current.value,
      };
      forgotPassword(formData);
      toast({
        position: "top-right",
        title: "Revise su correo para más instrucciones",
        status: "success",
        duration: 2000,
        isClosable: true,
      });
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al reiniciar la contraseña, ${error?.message}`,
        status: "error",
        duration: 2000,
        isClosable: true,
      });
    }

    setLoading(false);
  }
  return (
    <Flex width="full" align="center" justifyContent="center" minH="100vh">
      <Box
        p={8}
        borderWidth={1}
        borderRadius={8}
        style={{
          boxShadow:
            " 0 10px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)",
        }}
        mt={20}
        ml={4}
        mr={4}
        textAlign="center"
      >
        <Heading mb={8} fontSize="24px">
          REINICIAR CONTRASEÑA
        </Heading>

        <Box my={6} textAlign="left">
          <form onSubmit={handleSubmit}>
            <FormControl my={2} isRequired>
              <FormLabel>Email</FormLabel>
              <InputGroup>
                <InputLeftElement
                  pointerEvents="none"
                  children={
                    <MdEmail
                      color="gray"
                      size="1.5rem"
                      style={{ marginTop: "0.1rem" }}
                    />
                  }
                />
                <Input
                  type="email"
                  placeholder="user@gmail.com"
                  size="md"
                  style={{
                    boxShadow:
                      " 0 4px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)",
                  }}
                  ref={emailRef}
                />
              </InputGroup>
            </FormControl>
            <Button
              type="submit"
              width="full"
              onMouseEnter={() => setHover(true)}
              onMouseLeave={() => setHover(false)}
              style={{
                backgroundColor: `${onHover ? "#00a3c4" : "#2BD9A3"}`,
              }}
              color="white"
              mt={8}
              isLoading={loading}
              size="sm"
            >
              Reiniciar Contraseña
            </Button>

            <Link to="/signin">
              <Button
                type="button"
                size="sm"
                colorScheme="cyan"
                variant="outline"
                width="full"
                mt={4}
              >
                Iniciar Sesión
              </Button>
            </Link>
            <Box mt={8} textAlign="center">
              <Link
                to="/signup"
                onMouseEnter={() => setHover1(true)}
                onMouseLeave={() => setHover1(false)}
                style={{
                  color: `${onHover1 ? "#2BD9A3" : "black"}`,
                  textDecoration: `${onHover1 ? "underline" : "none"}`,
                }}
              >
                Registrarse
              </Link>
            </Box>
          </form>
        </Box>
      </Box>
    </Flex>
  );
};

export default ForgotPassword;
