import React, { useState, useRef } from "react";
//Components
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  InputGroup,
  InputLeftElement,
  useToast,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { MdEmail } from "react-icons/md";

// AWS Amplify
import { useAuth } from "../../context/AuthContext";

const ConfirmSignup = () => {
  let { confirmSignUp } = useAuth();
  //UI
  const [onHover1, setHover1] = useState(false);
  const toast = useToast();

  //User Data
  let username = useRef();
  let codeNumber = useRef();

  function handleSubmit(e) {
    e.preventDefault();
    try {
      let formData = {
        username: username.current.value,
        codeNumber: codeNumber.current.value,
        userType: "Client",
      };
      confirmSignUp(formData);
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al confirmar la cuenta, ${error?.message}`,
        status: "error",
        duration: 1500,
        isClosable: true,
      });
    }
  }
  return (
    <Flex
      minHeight="100vh"
      width="full"
      align="center"
      justifyContent="center"
      flexDirection="row"
    >
      <Box
        m={4}
        direction="row"
        borderRadius={8}
        boxShadow=" 0 10px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)"
      >
        <Box p={8} m="auto " maxW="md">
          <Heading textAlign="center" ontSize="32px">
            CONFIRMAR REGISTRO
          </Heading>
          <Box textAlign="left">
            <form onSubmit={handleSubmit}>
              <FormControl mt={8} isRequired>
                <FormLabel>Email</FormLabel>
                <InputGroup maxW="32rem">
                  <InputLeftElement
                    pointerEvents="none"
                    children={
                      <MdEmail
                        color="gray"
                        size="1.5rem"
                        style={{ marginTop: "0.1rem" }}
                      />
                    }
                  />
                  <Input
                    type="text"
                    placeholder="email"
                    size="md"
                    style={{
                      boxShadow:
                        " 0 4px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)",
                    }}
                    ref={username}
                  />
                </InputGroup>
              </FormControl>
              <FormControl isRequired mt={8}>
                <FormLabel>Confirm Code</FormLabel>
                <Input
                  type="number"
                  placeholder="confirmCode"
                  size="md"
                  style={{
                    boxShadow:
                      " 0 4px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)",
                  }}
                  ref={codeNumber}
                />
              </FormControl>

              <Box
                mt={8}
                display="flex"
                flexDirection="column"
                textAlign="center"
              >
                <Button
                  type="submit"
                  style={{ backgroundColor: "#2BD9A3" }}
                  variant="solid"
                  color="white"
                  w="16rem"
                  m="auto"
                >
                  CONFIRMAR
                </Button>
                <br />
                <Link
                  to="/signin"
                  onMouseEnter={() => setHover1(true)}
                  onMouseLeave={() => setHover1(false)}
                  style={{
                    fontSize: "0.9rem",
                    color: `${onHover1 ? "#2BD9A3" : "black"}`,
                    textDecoration: `${onHover1 ? "underline" : "none"}`,
                    marginTop: "1rem",
                  }}
                >
                  ¿Ya tiene una cuenta?
                </Link>
              </Box>
            </form>
          </Box>
        </Box>
      </Box>
    </Flex>
  );
};

export default ConfirmSignup;
