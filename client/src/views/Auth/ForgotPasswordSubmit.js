import React, { useState, useRef } from "react";

// AWS Amplify
import { useAuth } from "../../context/AuthContext";

//Components

import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  useToast,
} from "@chakra-ui/react";

//Media
import { MdEmail, MdLockOutline } from "react-icons/md";
import { IoMdEye, IoMdEyeOff } from "react-icons/io";

const ForgotPasswordSubmit = () => {
  //UI state
  const [show, setShow] = useState(true);
  const [onHover, setHover] = useState(false);

  //Autenticación
  let username = useRef();
  let password = useRef();
  let codeNumber = useRef();
  const { forgotPasswordSubmit } = useAuth();
  const [loading, setLoading] = useState(false);
  const toast = useToast();

  function handleSubmit(e) {
    e.preventDefault();

    try {
      let formData = {
        username: username.current.value,
        codeNumber: codeNumber.current.value,
        password: password.current.value,
      };
      forgotPasswordSubmit(formData);
      toast({
        position: "top-right",
        title: "Contraseña actualizada",
        status: "success",
        duration: 2000,
        isClosable: true,
      });
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al reiniciar la contraseña, ${error?.message}`,
        status: "error",
        duration: 2000,
        isClosable: true,
      });
    }

    setLoading(false);
  }
  return (
    <Flex width="full" align="center" justifyContent="center" minH="100vh">
      <Box
        p={8}
        borderWidth={1}
        borderRadius={8}
        style={{
          boxShadow:
            " 0 10px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)",
        }}
        mt={20}
        ml={4}
        mr={4}
        textAlign="center"
      >
        <Heading mb={8} fontSize="24px">
          REINICIAR CONTRASEÑA
        </Heading>

        <Box my={6} textAlign="left">
          <form onSubmit={handleSubmit}>
            <FormControl my={2} isRequired>
              <FormLabel>Email</FormLabel>
              <InputGroup>
                <InputLeftElement
                  pointerEvents="none"
                  children={
                    <MdEmail
                      color="gray"
                      size="1.5rem"
                      style={{ marginTop: "0.1rem" }}
                    />
                  }
                />
                <Input
                  type="email"
                  placeholder="user@gmail.com"
                  size="md"
                  style={{
                    boxShadow:
                      " 0 4px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)",
                  }}
                  ref={username}
                />
              </InputGroup>
            </FormControl>
            <FormControl mt={8} isRequired>
              <FormLabel>Codigo</FormLabel>
              <Input
                type="number"
                placeholder="confirmCode"
                size="md"
                style={{
                  boxShadow:
                    " 0 4px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)",
                }}
                ref={codeNumber}
              />
            </FormControl>
            <FormControl isRequired mt={6}>
              <FormLabel>New Password</FormLabel>
              <InputGroup>
                <InputLeftElement
                  pointerEvents="none"
                  children={
                    <MdLockOutline
                      size="1.5rem"
                      color="gray"
                      style={{ marginTop: "0.1rem" }}
                    />
                  }
                />
                <Input
                  type={show ? "password" : "text"}
                  placeholder="*******"
                  size="md"
                  style={{
                    boxShadow:
                      " 0 4px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)",
                  }}
                  ref={password}
                />
                <InputRightElement w="3.5rem">
                  <Button h="1.75rem" p="0" onClick={() => setShow(!show)}>
                    {show ? (
                      <IoMdEyeOff size="1.5rem" color="gray" />
                    ) : (
                      <IoMdEye size="1.5rem" color="gray" />
                    )}
                  </Button>
                </InputRightElement>
              </InputGroup>
            </FormControl>
            <Button
              type="submit"
              width="full"
              onMouseEnter={() => setHover(true)}
              onMouseLeave={() => setHover(false)}
              style={{
                backgroundColor: `${onHover ? "#00a3c4" : "#2BD9A3"}`,
              }}
              color="white"
              mt={8}
              isLoading={loading}
              size="sm"
            >
              Reiniciar Contraseña
            </Button>
          </form>
        </Box>
      </Box>
    </Flex>
  );
};

export default ForgotPasswordSubmit;
