import React, { useState } from "react";
import { useParams, useHistory } from "react-router";
import moment from "moment";
import { API, graphqlOperation, Storage } from "aws-amplify";
import { createHuesped } from "../graphql/mutations";
import Spinner from "../components/Spinner/Spinner";
//Components
import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
  Select,
  Text,
  SimpleGrid,
  InputGroup,
  useDisclosure,
  useToast,
} from "@chakra-ui/react";
import MomentUtils from "@date-io/moment";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";

const Checkin = () => {
  //UI state
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [onHover, setHover] = useState(false);

  const toast = useToast();

  const { isOpen, onOpen, onClose } = useDisclosure();
  let { id: idReserva, pax } = useParams();
  let totalPAX = parseInt(pax);
  const [selectedDate, setSelectedDate] = useState([
    ...Array(totalPAX).fill(moment()),
  ]);

  const handleDateChange = (e, idx) => {
    setSelectedDate((prev) => {
      let arr = [...prev];
      arr[idx] = e;
      return arr;
    });
  };
  let [userIMG, setUserIMG] = useState([...Array(totalPAX).fill(null)]);
  const handleImageInput = (idx, e) => {
    console.log(e.target.files);
    setUserIMG((prev) => {
      let arr = [...prev];
      arr[idx] = e.target.files[0];
      return arr;
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    let formData = new FormData(e.target);
    let inputData = [
      formData.getAll("NOMBRES"),
      formData.getAll("APELLIDOS"),
      formData.getAll("TIPDOC"),
      formData.getAll("NRODOC"),
      formData.getAll("FECNAC"),
      formData.getAll("GENERO"),
      formData.getAll("NACIONALIDAD"),
      formData.getAll("DIRECCION"),
      formData.getAll("TELEFONO"),
      formData.getAll("CELULAR"),
      formData.getAll("CELCTO"),
      formData.getAll("EMAIL"),
    ];

    try {
      setLoading(true);
      onOpen();
      for (let i = 0; i < totalPAX; i++) {
        await Storage.put(idReserva + formData.getAll("NRODOC")[i], userIMG[i]);
        let userData = {
          givenName: inputData[0][i],
          lastName: inputData[1][i],
          docType: inputData[2][i],
          docNumber: inputData[3][i],
          birthdate: inputData[4][i],
          gender: inputData[5][i],
          nationality: inputData[6][i],
          address: inputData[7][i],
          phone: inputData[8][i],
          cellphone: inputData[9][i],
          contact: inputData[10][i],
          email: inputData[11][i],
          reservaID: idReserva,
          type: "",
          docImg: idReserva + formData.getAll("NRODOC")[i],
        };

        await API.graphql(graphqlOperation(createHuesped, { input: userData }));
      }
      onClose();
      history.push("/");
      toast({
        position: "top-right",
        title: "Check In Completado",
        status: "success",
        duration: 1500,
        isClosable: true,
      });
    } catch (error) {
      toast({
        position: "top-right",
        title: `Error al realizar check in, ${error?.message}`,
        status: "error",
        duration: 2500,
        isClosable: true,
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      {loading && (
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalOverlay>
            <ModalContent>
              <ModalBody>
                <Spinner />
              </ModalBody>
            </ModalContent>
          </ModalOverlay>
        </Modal>
      )}
      <Flex
        minHeight="100vh"
        width="full"
        align="center"
        justifyContent="center"
        flexDirection="row"
        p={8}
      >
        <Flex
          w="container.lg"
          direction="column"
          borderRadius={8}
          p={8}
          boxShadow=" 0 10px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19)"
        >
          <Heading textAlign="center" mb={8}>
            Check In
          </Heading>
          <Heading fontSize="24px" lineHeight="25px" mb={8}>
            Información de los huéspedes
          </Heading>
          <form onSubmit={handleSubmit}>
            {totalPAX ? (
              [...Array(totalPAX)].map((item, idx) => (
                <div key={`h-${idx}`}>
                  <Text fontSize="xl" color="gray.900" fontWeight="bold">
                    Huésped {idx + 1}
                  </Text>
                  <SimpleGrid
                    columns={{ base: 1, sm: 1, md: 2, lg: 2, xl: 2 }}
                    spacing={8}
                    mb={8}
                  >
                    <FormControl isRequired>
                      <FormLabel>Nombres</FormLabel>
                      <Input placeholder="Nombres" name="NOMBRES" />
                    </FormControl>

                    <FormControl isRequired>
                      <FormLabel>Apellidos</FormLabel>
                      <Input placeholder="Apellidos" name="APELLIDOS" />
                    </FormControl>

                    <FormControl isRequired>
                      <FormLabel>Tipo de Documento</FormLabel>
                      <InputGroup>
                        <Select defaultValue="DNI" name="TIPDOC">
                          <option value="DNI">DNI</option>
                          <option value="CE">C. de E</option>
                        </Select>
                      </InputGroup>
                    </FormControl>

                    <FormControl isRequired>
                      <FormLabel>Nro. Documento</FormLabel>
                      <Input placeholder="Nro. Documento" name="NRODOC" />
                    </FormControl>

                    <FormControl isRequired>
                      <FormLabel>Fecha de nacimiento</FormLabel>
                      <MuiPickersUtilsProvider utils={MomentUtils}>
                        <KeyboardDatePicker
                          autoOk
                          variant="inline"
                          inputVariant="outlined"
                          value={selectedDate[idx]}
                          onChange={(e) => handleDateChange(e, idx)}
                          format="DD/MM/yyyy"
                          style={{ width: "100%" }}
                          name="FECNAC"
                        />
                      </MuiPickersUtilsProvider>
                    </FormControl>

                    <FormControl isRequired>
                      <FormLabel>Género</FormLabel>
                      <InputGroup>
                        <Select defaultValue="-" name="GENERO">
                          <option value="-">Elegir una opción</option>
                          <option value="M">Masculino</option>
                          <option value="F">Femenino</option>
                          <option value="O">Otro</option>
                        </Select>
                      </InputGroup>
                    </FormControl>

                    <FormControl isRequired>
                      <FormLabel>Nacionalidad</FormLabel>
                      <Input placeholder="País de origen" name="NACIONALIDAD" />
                    </FormControl>

                    <FormControl isRequired>
                      <FormLabel>Dirección</FormLabel>
                      <Input placeholder="Calle/Avenida #" name="DIRECCION" />
                    </FormControl>

                    <FormControl isRequired>
                      <FormLabel>Teléfono</FormLabel>
                      <Input placeholder="Línea fija" name="TELEFONO" />
                    </FormControl>

                    <FormControl isRequired>
                      <FormLabel>Celular</FormLabel>
                      <Input placeholder="Teléfono móvil" name="CELULAR" />
                    </FormControl>

                    <FormControl isRequired>
                      <FormLabel>Número de contacto</FormLabel>
                      <Input
                        placeholder="Contacto de emergencia"
                        name="CELCTO"
                      />
                    </FormControl>

                    <FormControl isRequired>
                      <FormLabel>Email</FormLabel>
                      <Input placeholder="Email" name="EMAIL" />
                    </FormControl>

                    <FormControl isRequired>
                      <FormLabel>
                        Escaneo o foto del documento de identidad (Ambas caras
                        del documento)
                      </FormLabel>
                      <Input
                        type="file"
                        name="IMAGE"
                        onChange={(e) => handleImageInput(idx, e)}
                        pt={1.5}
                      />
                    </FormControl>
                  </SimpleGrid>
                </div>
              ))
            ) : (
              <></>
            )}
            <Button
              type="submit"
              onMouseEnter={() => setHover(true)}
              onMouseLeave={() => setHover(false)}
              style={{
                backgroundColor: `${onHover ? "#00a3c4" : "#2BD9A3"}`,
              }}
              variant="solid"
              color="white"
              w="8rem"
              m="auto"
              size="sm"
            >
              Registrar
            </Button>
          </form>
        </Flex>
      </Flex>
    </>
  );
};

export default Checkin;
