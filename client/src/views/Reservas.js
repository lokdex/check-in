import React, { useEffect, useState } from "react";
import { Badge, Box, Button, Flex, Heading } from "@chakra-ui/react";
import { StarIcon } from "@chakra-ui/icons";
import { Link } from "react-router-dom";
import { API, graphqlOperation } from "aws-amplify";
import { useAuth } from "../context/AuthContext";
import Spinner from "../components/Spinner/Spinner";
import { listReservas } from "../graphql/queries";

const Reservas = () => {
  const [reservas, setReservas] = useState([]);
  let { user } = useAuth();
  let email = user?.attributes?.email;

  const property = {
    reviewCount: 34,
    rating: 4,
  };

  async function fetchReservasByEmail() {
    let { data } = await API.graphql(
      graphqlOperation(listReservas, { filter: { clienteMail: { eq: email } } })
    );
    setReservas(data?.listReservas?.items);
  }

  useEffect(() => {
    fetchReservasByEmail();
  }, []);

  return (
    <Box p={4} minH="100vh">
      <Heading fontSize="24px">Reservas</Heading>
      {reservas ? (
        !reservas.length ? (
          <Spinner />
        ) : (
          reservas.map(reserva => (
            <Flex
              wrap="wrap"
              borderWidth="1px"
              borderRadius="lg"
              overflow="hidden"
              m={8}
              key={reserva?.id}
            >
              <Box p="6">
                <Box d="flex" alignItems="baseline">
                  <Badge borderRadius="full" px="2" colorScheme="teal">
                    New
                  </Badge>
                  <Box
                    color="gray.500"
                    fontWeight="semibold"
                    letterSpacing="wide"
                    fontSize="xs"
                    textTransform="uppercase"
                    ml="2"
                  >
                    {reserva?.roomQty} habitación(es)
                  </Box>
                </Box>

                <Box
                  mt="1"
                  fontWeight="semibold"
                  as="h4"
                  lineHeight="tight"
                  isTruncated
                >
                  {reserva?.comments}
                </Box>

                <Box color="black" fontWeight="bold">
                  S/.
                  {reserva?.tarifaTotal + " "}
                  <Box as="span" color="black" fontWeight="bold" fontSize="sm">
                    Tarifa total
                  </Box>
                </Box>

                <Box d="flex" mt="2" alignItems="center">
                  {Array(5)
                    .fill("")
                    .map((_, i) => (
                      <StarIcon
                        key={i}
                        color={i < property.rating ? "teal.500" : "gray.300"}
                      />
                    ))}
                </Box>
              </Box>
              <Flex ml="auto" mt="auto" wrap="wrap" alignItems="flex-end">
                <Link to={`/reserva-detalle/${reserva?.id}`}>
                  <Button
                    type="button"
                    size="sm"
                    colorScheme="cyan"
                    variant="outline"
                    mb={4}
                    mr={4}
                  >
                    Detalle
                  </Button>
                </Link>

                {reserva.status === "R" ? (
                  <Link
                    to={`/checkin/${reserva?.id}/${reserva?.totalPAX}`}
                    style={{ fontWeight: "bold" }}
                  >
                    <Button
                      style={{ backgroundColor: "#2BD9A3" }}
                      variant="solid"
                      color="white"
                      size="sm"
                      mb={4}
                      mr={4}
                    >
                      CheckIn
                    </Button>
                  </Link>
                ) : (
                  <></>
                )}
              </Flex>
            </Flex>
          ))
        )
      ) : (
        <Box mt={8}>No posee reservas</Box>
      )}
    </Box>
  );
};

export default Reservas;
