import React from "react";
import { Box, Button, Flex, Heading, Image, SimpleGrid, Text } from "@chakra-ui/react";
import Carousel from "../components/Carousel/Carousel";
//Images
import CarouselMultiItem from "../components/CaoruselMultiItem/CarouselMultiItem";

const Home = () => {
   const images = [
      "https://images.pexels.com/photos/2259226/pexels-photo-2259226.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
      "https://images.pexels.com/photos/2470607/pexels-photo-2470607.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
      "https://images.pexels.com/photos/3081485/pexels-photo-3081485.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260",
   ];

   const headers = [
      "El lugar perfecto para reír, disfrutar y soñar",
      "Ciertos lugares te hacen vivir momentos únicos",
      "Una experiencia para dormir de 5 estrellas a un precio de 1 estrella.",
   ];

   const buttonsText = ["Registrarse ahora", "Conocer más"];
   const links = ["/signup", "/services"];
   const cards = [
      {
         id: "c1",
         title: "Turismo",
         subtitle:
            "Explore las maravillas de Perú con un viaje desde Lima hasta Cusco y Machu Picchu, o deguste delicias durante su camino a través de Italia. Con nuestra colección de experiencias de viaje inolvidables, lo podemos ayudar a crear su itinerario soñado.",
         image: "https://images.pexels.com/photos/532803/pexels-photo-532803.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
         buttonText: "Descubrir",
         link: "/algo1",
      },
      {
         id: "c2",
         title: "Habitaciones",
         subtitle:
            "Despierte temprano en un antiguo monasterio en el centro de Cusco y emprenda una excursión en tren a Machu Picchu. O disfrute en el hotel con más estrellas de Río antes de explorar la selva tropical de Iguazú. América del Sur está llena de destinos: ¿a cuál de todos ellos irá?",
         image: "https://images.pexels.com/photos/6480209/pexels-photo-6480209.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260",
         buttonText: "Descubrir",
         link: "/algo2",
      },
      {
         id: "c3",
         title: "Restaurantes",
         subtitle:
            "Disfrute de una gastronomía extraordinaria. Desde un icónico restaurante en Nueva York hasta rincones gastronómicos con estrella Michelin en Inglaterra, Madeira, Brasil e Italia, tenemos algo para todos los paladares.",
         image: "https://images.pexels.com/photos/9392764/pexels-photo-9392764.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
         buttonText: "Descubrir",
         link: "/algo3",
      },
   ];

   const articles = [
      {
         id: "a1",
         title: "COMIDAS",
         subtitle: "DESAYUNOS - ALMUERZOS - CENAS",
         image: "https://images.pexels.com/photos/2566032/pexels-photo-2566032.jpeg?cs=srgb&dl=pexels-sander-dalhuisen-2566032.jpg&fm=jpg",
         buttonText: "Descubrir",
         link: "/algo4",
      },
      {
         id: "a2",
         title: "TRABAJO",
         subtitle: "ESPACIOS - CONCENTRATE",
         image: "https://images.pexels.com/photos/733856/pexels-photo-733856.jpeg?cs=srgb&dl=pexels-tirachard-kumtanom-733856.jpg&fm=jpg",
         buttonText: "Descubrir",
         link: "/algo5",
      },
      {
         id: "a3",
         title: "MÚSICA",
         subtitle: "DISFRUTA TODO TIPO DE GÉNERO",
         image: "https://images.pexels.com/photos/3642350/pexels-photo-3642350.jpeg?cs=srgb&dl=pexels-laura-balbarde-3642350.jpg&fm=jpg",
         buttonText: "Descubrir",
         link: "/algo6",
      },
      {
         id: "a4",
         title: "FOTOGRAFÍA",
         subtitle: "RESERVA SESIONES JUNTO A TUS SERES QUERIDOS",
         image: "https://images.pexels.com/photos/5808388/pexels-photo-5808388.jpeg?cs=srgb&dl=pexels-lisa-5808388.jpg&fm=jpg",
         buttonText: "Descubrir",
         link: "/algo7",
      },
      {
         id: "a5",
         title: "VIAJES",
         subtitle: "AGENDA CON ANTICIPACIÓN",
         image: "https://images.pexels.com/photos/163688/hiker-travel-trip-wander-163688.jpeg?cs=srgb&dl=pexels-pixabay-163688.jpg&fm=jpg",
         buttonText: "Descubrir",
         link: "/algo8",
      },
   ];

   return (
      <Box>
         <Box as="section">
            <Carousel headers={headers} images={images} buttonsText={buttonsText} links={links} />
         </Box>
         <Box as="section" p={16} justifyContent="center" alignContent="center">
            <Heading
               mt={2}
               fontSize="24px"
               textAlign="center"
               lineHeight="tight"
               fontWeight="normal"
            >
               Servicios
            </Heading>

            <Text m={8} fontWeight="" as="h4" textAlign="center" lineHeight="tight" isTruncated>
               Disfruta tu estadía con la mejor atención
            </Text>
            <SimpleGrid
               m="auto"
               maxW="64rem"
               columns={{ base: 1, sm: 1, md: 2, lg: 3, xl: 3 }}
               spacing={4}
            >
               {cards.map((card) => (
                  <Box
                     maxW="xs"
                     key={card?.id}
                     m="auto"
                     overflow="hidden"
                     boxShadow="0 0 0 0 rgb(0 0 0 / 16%)"
                     position="static"
                  >
                     <Image
                        src={card?.image}
                        boxSize="xs"
                        objectFit="cover"
                        maxH="20rem"
                        position="relative"
                        zIndex="1"
                     />
                     <Box
                        p="6"
                        overflow="hidden"
                        boxSizing="border-box"
                        fontSize="12px"
                        maxH="10rem"
                        position="relative"
                        zIndex="999"
                        mb="0"
                        transition="all 0.75s ease-in-out"
                        _hover={{
                           transition: "all 0.75s ease-in-out",
                           maxH: "16.2rem",
                        }}
                     >
                        <Heading
                           mt="4"
                           as="h4"
                           textAlign="center"
                           lineHeight="tight"
                           fontSize="16px"
                           fontWeight="700"
                           textTransform="uppercase"
                           letterSpacing="0.25rem"
                        >
                           {card?.title}
                        </Heading>

                        <Heading
                           mt="1"
                           size="md"
                           textAlign="center"
                           fontSize="16px"
                           lineHeight="tight"
                           fontWeight="normal"
                        >
                           {card?.subtitle}
                        </Heading>
                     </Box>
                     <Box
                        mt="4"
                        as="h4"
                        textAlign="center"
                        lineHeight="tight"
                        isTruncated
                        textTransform="uppercase"
                        letterSpacing="0.5rem"
                     >
                        <Button colorScheme="blackAlpha.800" variant="link">
                           {card?.buttonText}
                        </Button>
                     </Box>
                  </Box>
               ))}
            </SimpleGrid>
         </Box>
         <Flex bg="#EDF2F7" as="section" m={4}>
            <CarouselMultiItem articles={articles} />
         </Flex>
      </Box>
   );
};

export default Home;
