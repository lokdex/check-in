import React, { useState } from "react";

//Components
import Slider from "react-slick";
import { Box, Button, Center, Container, Heading, Image, IconButton } from "@chakra-ui/react";

//Special imports
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./CarouselMultiItem.css";
//Icons
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from "react-icons/md";

const CarouselMultiItem = (props) => {
   const ArrowLeft = ({ onClick }) => (
      <div
         style={{
            zIndex: "10",
            position: "absolute",
            top: "50%",
            left: "-4%",
            transform: "translate(0,-50%)",
         }}
         onClick={onClick}
      >
         <IconButton
            colorScheme="blackAlpha"
            size="sm"
            justifyContent="center"
            icon={<MdKeyboardArrowLeft />}
            isRound
         />
      </div>
   );
   const ArrowRight = ({ onClick }) => (
      <div
         onClick={onClick}
         style={{
            zIndex: "10",
            position: "absolute",
            top: "50%",
            right: "-4%",
            transform: "translate(0,-50%)",
         }}
      >
         <IconButton
            colorScheme="blackAlpha"
            size="sm"
            justifyContent="center"
            onClick={onClick}
            icon={<MdKeyboardArrowRight />}
            isRound
         />
      </div>
   );

   const [imageIndex, setImageIndex] = useState(0);
   const settings = {
      infinite: true,
      lazyLoad: true,
      speed: 500,
      slidesToShow: 3,
      centerMode: true,
      centerPadding: 0,
      swipeToSlide: true,
      beforeChange: (current, next) => setImageIndex(next),

      responsive: [
         {
            breakpoint: 1024,
            settings: {
               slidesToShow: 2,
               slidesToScroll: 1,
               infinite: true,
            },
         },
         {
            breakpoint: 768,
            settings: {
               slidesToShow: 2,
               slidesToScroll: 1,
               infinite: true,
            },
         },
         {
            breakpoint: 600,
            settings: {
               slidesToShow: 1,
               slidesToScroll: 1,
               infinite: true,
            },
         },

         {
            breakpoint: 480,
            settings: {
               slidesToShow: 1,
               slidesToScroll: 1,
               autoplay: true,
            },
         },
      ],
      prevArrow: <ArrowLeft />,
      nextArrow: <ArrowRight />,
   };
   return (
      <Container maxW="container.lg" justifyContent="center" p={4}>
         <Slider {...settings}>
            {props.articles.map((article, idx) => (
               <Center
                  key={article?.id}
                  className={idx === imageIndex ? "slide activeSlide" : "slide"}
               >
                  <Box maxW="xs" overflow="hidden" bg="white" m="auto">
                     <Image src={article?.image} boxSize="xs" objectFit="cover" maxH="20rem" />
                     <Box p={8} position="relative">
                        <Heading
                           mt="1"
                           size="md"
                           textAlign="center"
                           lineHeight="tight"
                           fontWeight="light"
                        >
                           {article?.title}
                        </Heading>

                        <Heading
                           mt="4"
                           fontWeight="semibold"
                           textAlign="center"
                           lineHeight="tight"
                           size="sm"
                           isTruncated
                           textTransform="uppercase"
                           letterSpacing="0.25rem"
                        >
                           {article?.subtitle}
                        </Heading>
                        <Box
                           mt="4"
                           as="h4"
                           textAlign="center"
                           lineHeight="tight"
                           isTruncated
                           textTransform="uppercase"
                           letterSpacing="0.5rem"
                        >
                           <Button colorScheme="blackAlpha.800" variant="link">
                              {article?.buttonText}
                           </Button>
                        </Box>
                     </Box>
                  </Box>
               </Center>
            ))}
         </Slider>
      </Container>
   );
};

export default CarouselMultiItem;
