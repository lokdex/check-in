import React from "react";
//Components
import { Divider, Flex, Grid, GridItem, Image, Link, Stack, Text } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";

//Icons
import { FaInstagram, FaFacebookF, FaTwitter } from "react-icons/fa";

//Media
import corregidor from "../../assets/images/logos/corregidor.svg";
import alborit from "../../assets/images/logos/alborit.svg";
import sanpablo from "../../assets/images/logos/san_pablo.svg";

function Footer(props) {
   return (
      <Flex
         as="footer"
         align="center"
         justify="center"
         direction="row"
         wrap="wrap"
         minH="18rem"
         pb={4}
         justifyContent="center"
         bg="#333"
         w="100%"
      >
         <Stack
            spacing={{ base: "4rem", sm: "4rem", md: "4rem", lg: "8rem", xl: "10rem" }}
            align="center"
            justify="center"
            w="100%"
            p={10}
            direction={{ sm: "column", base: "column", md: "row" }}
         >
            <RouterLink to="/">
               <Image src={corregidor} alt="corregidor" maxW="8rem" h="10rem" />
            </RouterLink>
            <Grid color="white" h="14rem" gap={{ sm: 2, md: 3, lg: 3 }}>
               <GridItem>
                  <Text as="p" fontSize="md" fontWeight="bold">
                     Nosotros
                  </Text>
               </GridItem>

               <Divider />
               <GridItem>
                  <Text as="p" fontSize="sm">
                     Contáctenos
                  </Text>
               </GridItem>
               <Divider />
               <GridItem>
                  <Text as="p" fontSize="sm">
                     Llámanos
                  </Text>
                  <Text fontSize="xs" textAlign="center">
                     +51 999 999 999
                  </Text>
               </GridItem>
               <Divider />
               <GridItem>
                  <Text as="p" fontSize="sm">
                     Envíanos un Mail
                  </Text>
                  <Text fontSize="xs" textAlign="center">
                     correo@gmail.com
                  </Text>
               </GridItem>
            </Grid>
            <Grid
               h="14rem"
               gap={{ sm: 2, md: 3, lg: 3 }}
               align="center"
               justify="center"
               textAlign="center"
               color="white"
            >
               <GridItem>
                  <Text fontSize="md" fontWeight="bold">
                     Encuentranos en:
                  </Text>
               </GridItem>
               <GridItem>
                  <Link href="https://www.instagram.com" target="blank">
                     <FaInstagram
                        style={{
                           float: "left",
                           marginRight: "0.5rem",
                           marginTop: "0.1rem",
                        }}
                        color="white"
                     />
                     <Text as="p" fontSize="sm">
                        Instagram
                     </Text>
                  </Link>
               </GridItem>
               <Divider />
               <GridItem>
                  <Link href="https://www.facebook.com" target="blank">
                     <FaFacebookF
                        style={{
                           float: "left",
                           marginRight: "0.5rem",
                           marginTop: "0.1rem",
                        }}
                        color="white"
                     />
                     <Text as="p" fontSize="sm">
                        Facebook
                     </Text>
                  </Link>
               </GridItem>
               <Divider />
               <GridItem>
                  <Link href="https://twitter.com" target="blank">
                     <FaTwitter
                        style={{
                           float: "left",
                           marginRight: "0.5rem",
                           marginTop: "0.1rem",
                        }}
                        color="white"
                     />
                     <Text as="p" fontSize="sm">
                        Twitter
                     </Text>
                  </Link>
               </GridItem>
            </Grid>
            <Grid
               h="14rem"
               gap={{ base: 6, sm: 4, md: 5, lg: 5 }}
               align="center"
               justify="center"
               color="white"
            >
               <GridItem>
                  <Text fontSize="md" fontWeight="bold">
                     Con el apoyo de :
                  </Text>
               </GridItem>

               <GridItem>
                  <Link href="https://alborit.com/" target="blank">
                     <Image src={alborit} alt="alborit" maxW="10rem" />
                  </Link>
               </GridItem>
               <GridItem>
                  <Link href="https://ucsp.edu.pe/" target="blank">
                     <Image src={sanpablo} alt="sanpablo" maxW="10rem" />
                  </Link>
               </GridItem>
            </Grid>
         </Stack>
         <Divider mt={2} />
         <Link color="white" href="/">
            <Text mt={2}>© 2021 - WebCheck</Text>
         </Link>
      </Flex>
   );
}
export default Footer;
