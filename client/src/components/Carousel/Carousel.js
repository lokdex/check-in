import React from "react";

//Components
import Slider from "react-slick";
import { IconButton } from "@chakra-ui/react";
import Slide from "./Slide";

//Special imports
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

//Media
//Icons
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from "react-icons/md";

import "./Carousel.css";

const Carousel = (props) => {
   const ArrowLeft = ({ onClick }) => (
      <div
         style={{
            zIndex: "10",
            position: "absolute",
            top: "50%",
            left: "1.5%",
            transform: "translate(0,-50%)",
            display: "none",
         }}
         onClick={onClick}
      >
         <IconButton
            colorScheme="blackAlpha"
            size="lg"
            justifyContent="center"
            icon={<MdKeyboardArrowLeft />}
            isRound
         />
      </div>
   );
   const ArrowRight = ({ onClick }) => (
      <div
         onClick={onClick}
         style={{
            zIndex: "10",
            position: "absolute",
            top: "50%",
            right: "1.5%",
            transform: "translate(0,-50%)",
            display: "none",
         }}
      >
         <IconButton
            colorScheme="blackAlpha"
            size="lg"
            justifyContent="center"
            onClick={onClick}
            icon={<MdKeyboardArrowRight />}
            isRound
         />
      </div>
   );

   const settings = {
      infinite: true,

      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      autoplay: true,
      speed: 1000,
      autoplaySpeed: 5000,
      prevArrow: <ArrowLeft />,
      nextArrow: <ArrowRight />,
   };

   return (
      <Slider {...settings}>
         {props.images.map((img, id) => (
            <Slide key={id} id={id} image={img} header={props.headers[id]} />
         ))}
      </Slider>
   );
};

export default Carousel;
