import React from "react";
import { Box, Center, Flex, Text } from "@chakra-ui/react";

const Slide = (props) => {
   return (
      <Box as="div">
         <Box
            zIndex="1"
            height={{
               base: "20rem",
               sm: "20rem",
               md: "25rem",
               lg: "40rem",
            }}
            backgroundSize="cover"
            width="100%"
            backgroundRepeat="no-repeat"
            position="relative"
            backgroundImage={`url(${props.image})`}
            textAlign="center"
         >
            <Box position="absolute" h="100%" w="100%" background="black" opacity="0.5" />
            <Center
               h={{
                  base: "22rem",
                  sm: "22rem",
                  md: "24rem",
                  lg: "40rem",
                  xl: "40rem",
               }}
            >
               <Flex direction="column" justifyContent="center" alignItems="center">
                  <Text
                     fontSize={{
                        base: "lg",
                        sm: "lg",
                        md: "2xl",
                        lg: "4xl",
                        xl: "3xl",
                     }}
                     color="white"
                     fontWeight="medium"
                     textTransform="uppercase"
                     zIndex="10"
                     pl={8}
                     pr={8}
                  >
                     {props.header}
                  </Text>
               </Flex>
            </Center>
         </Box>
      </Box>
   );
};

export default Slide;
