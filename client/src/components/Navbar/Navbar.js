import React from "react";
import {
  Box,
  Button,
  IconButton,
  Flex,
  Divider,
  Drawer,
  DrawerOverlay,
  DrawerContent,
  DrawerHeader,
  DrawerBody,
  useDisclosure,
  DrawerCloseButton,
  Stack,
  Heading,
} from "@chakra-ui/react";
import { NavLink, withRouter } from "react-router-dom";
import { useAuth } from "../../context/AuthContext";

//Media
import "./Navbar.css";
//Icons

import { MdMenu } from "react-icons/md";
import { FaHome, FaSignOutAlt } from "react-icons/fa";
import { MdLockOpen, MdDateRange, MdDoneAll } from "react-icons/md";

const Navbar = props => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  let { user } = useAuth();
  let { signOut } = useAuth();

  return props?.location?.pathname === "/signin" ||
    props.location?.pathname === "/signup" ||
    props.location?.pathname === "/confirm-signup" ||
    props.location?.pathname === "/forgot-password" ||
    props.location?.pathname === "/forgot-submit" ? null : (
    <Flex
      as="nav"
      align="center"
      justify="space-between"
      wrap="wrap"
      w="100%"
      p={4}
      bg="blackAlpha.800"
      color="white"
      textAlign="center"
      {...props}
      position="absolute"
      zIndex="10"
      h="4rem"
    >
      <Flex display="table">
        <Box
          display={{
            base: "table-cell",
            md: "table-cell",
            lg: "none",
            xl: "none",
          }}
          onClick={onOpen}
        >
          <IconButton background={"transparent"} size="sm" icon={<MdMenu />} />
        </Box>
        <Box paddingStart={4} display="table-cell" verticalAlign="middle">
          <NavLink to="/">
            <Heading fontSize="20px" lineHeight="21px">
              Web Check In
            </Heading>
          </NavLink>
        </Box>
      </Flex>

      <Box
        display={{ base: "none", md: "none", lg: "table", xl: "table" }}
        pr={4}
      >
        <Stack
          align="center"
          justify="flex-end"
          direction="row"
          textAlign="center"
          spacing={8}
        >
          <NavLink
            className="link"
            style={{ verticalAlign: "middle" }}
            to="/"
            exact={true}
          >
            Inicio
          </NavLink>
          {!user ? null : user?.userType === "CL" ? (
            <NavLink
              className="link"
              style={{ verticalAlign: "middle" }}
              to="/reservas"
            >
              Mis Reservas
            </NavLink>
          ) : (
            <NavLink
              className="link"
              style={{ verticalAlign: "middle" }}
              to="/handle-checkin"
            >
              Gst. Check In
            </NavLink>
          )}

          {!user ? (
            <NavLink
              className="link"
              style={{ verticalAlign: "middle" }}
              to="/signin"
            >
              Acceder
            </NavLink>
          ) : (
            <Button
              colorScheme="blackAlpha"
              onClick={signOut}
              fontSize="12px"
              textTransform="upperCase"
              size="sm"
            >
              Cerrar Sesión
            </Button>
          )}
        </Stack>
      </Box>

      <Drawer placement={"left"} onClose={onClose} isOpen={isOpen}>
        <DrawerOverlay>
          <DrawerContent bg="blackAlpha.900" color="white" onClick={onClose}>
            <DrawerCloseButton />
            <DrawerHeader>
              <Flex>
                <Box maxW="sm" maxH="1rem">
                  <NavLink to="/" onClick={onClose}>
                    <Heading size="lg">WebCheck</Heading>
                  </NavLink>
                </Box>
              </Flex>
            </DrawerHeader>
            <DrawerBody mt={"2rem"}>
              <Stack direction={"column"} spacing={6}>
                <Stack direction={"row"}>
                  <FaHome style={{ marginTop: "0.2rem" }} />
                  <NavLink to="/">Inicio</NavLink>
                </Stack>
                <Divider />
                {!user && user?.userType === "CL" ? null : (
                  <>
                    <Stack direction={"row"}>
                      <MdDateRange style={{ marginTop: "0.2rem" }} />
                      <NavLink to="/reservas">Mis Reservas</NavLink>
                    </Stack>

                    <Divider />
                  </>
                )}
                {user?.userType === "CL" ? null : (
                  <>
                    <Stack direction={"row"}>
                      <MdDoneAll style={{ marginTop: "0.2rem" }} />
                      <NavLink to="/handle-checkin">Gst. Check In</NavLink>
                    </Stack>
                    <Divider />
                  </>
                )}

                {!user ? (
                  <Stack direction={"row"}>
                    <MdLockOpen style={{ marginTop: "0.2rem" }} />
                    <NavLink to="/signin">Iniciar Sesión</NavLink>
                  </Stack>
                ) : (
                  <Button
                    leftIcon={<FaSignOutAlt />}
                    onClick={signOut}
                    bg="#303030"
                    _hover={{ bg: "#4A5568" }}
                    color="white"
                    variant="solid"
                  >
                    Cerrar Sesión
                  </Button>
                )}
              </Stack>
            </DrawerBody>
          </DrawerContent>
        </DrawerOverlay>
      </Drawer>
    </Flex>
  );
};

export default withRouter(Navbar);
