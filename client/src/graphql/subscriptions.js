/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateTarifa = /* GraphQL */ `
  subscription OnCreateTarifa {
    onCreateTarifa {
      id
      description
      type
      status
      value
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateTarifa = /* GraphQL */ `
  subscription OnUpdateTarifa {
    onUpdateTarifa {
      id
      description
      type
      status
      value
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteTarifa = /* GraphQL */ `
  subscription OnDeleteTarifa {
    onDeleteTarifa {
      id
      description
      type
      status
      value
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateHabitacion = /* GraphQL */ `
  subscription OnCreateHabitacion {
    onCreateHabitacion {
      id
      number
      type
      description
      status
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      ctaHabitacionID
      ctaHabitacion {
        id
        habitacionID
        habitacion {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        date
        cargos
        abonos
        saldos
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateHabitacion = /* GraphQL */ `
  subscription OnUpdateHabitacion {
    onUpdateHabitacion {
      id
      number
      type
      description
      status
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      ctaHabitacionID
      ctaHabitacion {
        id
        habitacionID
        habitacion {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        date
        cargos
        abonos
        saldos
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteHabitacion = /* GraphQL */ `
  subscription OnDeleteHabitacion {
    onDeleteHabitacion {
      id
      number
      type
      description
      status
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      ctaHabitacionID
      ctaHabitacion {
        id
        habitacionID
        habitacion {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        date
        cargos
        abonos
        saldos
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateHuesped = /* GraphQL */ `
  subscription OnCreateHuesped {
    onCreateHuesped {
      id
      docType
      docNumber
      docImg
      type
      givenName
      lastName
      address
      birthdate
      nationality
      gender
      phone
      cellphone
      email
      contact
      comments
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateHuesped = /* GraphQL */ `
  subscription OnUpdateHuesped {
    onUpdateHuesped {
      id
      docType
      docNumber
      docImg
      type
      givenName
      lastName
      address
      birthdate
      nationality
      gender
      phone
      cellphone
      email
      contact
      comments
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteHuesped = /* GraphQL */ `
  subscription OnDeleteHuesped {
    onDeleteHuesped {
      id
      docType
      docNumber
      docImg
      type
      givenName
      lastName
      address
      birthdate
      nationality
      gender
      phone
      cellphone
      email
      contact
      comments
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateCliente = /* GraphQL */ `
  subscription OnCreateCliente {
    onCreateCliente {
      id
      docType
      docNumber
      type
      givenName
      lastName
      businessName
      address
      phone
      cellphone
      email
      contact
      comments
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateCliente = /* GraphQL */ `
  subscription OnUpdateCliente {
    onUpdateCliente {
      id
      docType
      docNumber
      type
      givenName
      lastName
      businessName
      address
      phone
      cellphone
      email
      contact
      comments
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteCliente = /* GraphQL */ `
  subscription OnDeleteCliente {
    onDeleteCliente {
      id
      docType
      docNumber
      type
      givenName
      lastName
      businessName
      address
      phone
      cellphone
      email
      contact
      comments
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateReserva = /* GraphQL */ `
  subscription OnCreateReserva {
    onCreateReserva {
      id
      clienteID
      clienteMail
      cliente {
        id
        docType
        docNumber
        type
        givenName
        lastName
        businessName
        address
        phone
        cellphone
        email
        contact
        comments
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      huespedes {
        items {
          id
          docType
          docNumber
          docImg
          type
          givenName
          lastName
          address
          birthdate
          nationality
          gender
          phone
          cellphone
          email
          contact
          comments
          reservaID
          createdAt
          updatedAt
        }
        nextToken
      }
      nightQty
      roomQty
      habitaciones {
        items {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        nextToken
      }
      dateIn
      dateOut
      paymentMethod
      pensionType
      status
      adultPAX
      kidPAX
      babyPAX
      totalPAX
      PAXinside
      tarifaID
      tarifa {
        id
        description
        type
        status
        value
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      tarifaTotal
      comments
      createdBy
      updatedBy
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateReserva = /* GraphQL */ `
  subscription OnUpdateReserva {
    onUpdateReserva {
      id
      clienteID
      clienteMail
      cliente {
        id
        docType
        docNumber
        type
        givenName
        lastName
        businessName
        address
        phone
        cellphone
        email
        contact
        comments
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      huespedes {
        items {
          id
          docType
          docNumber
          docImg
          type
          givenName
          lastName
          address
          birthdate
          nationality
          gender
          phone
          cellphone
          email
          contact
          comments
          reservaID
          createdAt
          updatedAt
        }
        nextToken
      }
      nightQty
      roomQty
      habitaciones {
        items {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        nextToken
      }
      dateIn
      dateOut
      paymentMethod
      pensionType
      status
      adultPAX
      kidPAX
      babyPAX
      totalPAX
      PAXinside
      tarifaID
      tarifa {
        id
        description
        type
        status
        value
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      tarifaTotal
      comments
      createdBy
      updatedBy
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteReserva = /* GraphQL */ `
  subscription OnDeleteReserva {
    onDeleteReserva {
      id
      clienteID
      clienteMail
      cliente {
        id
        docType
        docNumber
        type
        givenName
        lastName
        businessName
        address
        phone
        cellphone
        email
        contact
        comments
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      huespedes {
        items {
          id
          docType
          docNumber
          docImg
          type
          givenName
          lastName
          address
          birthdate
          nationality
          gender
          phone
          cellphone
          email
          contact
          comments
          reservaID
          createdAt
          updatedAt
        }
        nextToken
      }
      nightQty
      roomQty
      habitaciones {
        items {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        nextToken
      }
      dateIn
      dateOut
      paymentMethod
      pensionType
      status
      adultPAX
      kidPAX
      babyPAX
      totalPAX
      PAXinside
      tarifaID
      tarifa {
        id
        description
        type
        status
        value
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      tarifaTotal
      comments
      createdBy
      updatedBy
      createdAt
      updatedAt
    }
  }
`;
export const onCreateCuentaHabitacion = /* GraphQL */ `
  subscription OnCreateCuentaHabitacion {
    onCreateCuentaHabitacion {
      id
      habitacionID
      habitacion {
        id
        number
        type
        description
        status
        reservaID
        reserva {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        ctaHabitacionID
        ctaHabitacion {
          id
          habitacionID
          date
          cargos
          abonos
          saldos
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      date
      cargos
      abonos
      saldos
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateCuentaHabitacion = /* GraphQL */ `
  subscription OnUpdateCuentaHabitacion {
    onUpdateCuentaHabitacion {
      id
      habitacionID
      habitacion {
        id
        number
        type
        description
        status
        reservaID
        reserva {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        ctaHabitacionID
        ctaHabitacion {
          id
          habitacionID
          date
          cargos
          abonos
          saldos
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      date
      cargos
      abonos
      saldos
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteCuentaHabitacion = /* GraphQL */ `
  subscription OnDeleteCuentaHabitacion {
    onDeleteCuentaHabitacion {
      id
      habitacionID
      habitacion {
        id
        number
        type
        description
        status
        reservaID
        reserva {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        ctaHabitacionID
        ctaHabitacion {
          id
          habitacionID
          date
          cargos
          abonos
          saldos
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      date
      cargos
      abonos
      saldos
      createdAt
      updatedAt
    }
  }
`;
export const onCreateUsuario = /* GraphQL */ `
  subscription OnCreateUsuario {
    onCreateUsuario {
      id
      email
      type
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateUsuario = /* GraphQL */ `
  subscription OnUpdateUsuario {
    onUpdateUsuario {
      id
      email
      type
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteUsuario = /* GraphQL */ `
  subscription OnDeleteUsuario {
    onDeleteUsuario {
      id
      email
      type
      createdAt
      updatedAt
    }
  }
`;
