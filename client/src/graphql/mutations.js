/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createTarifa = /* GraphQL */ `
  mutation CreateTarifa(
    $input: CreateTarifaInput!
    $condition: ModelTarifaConditionInput
  ) {
    createTarifa(input: $input, condition: $condition) {
      id
      description
      type
      status
      value
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateTarifa = /* GraphQL */ `
  mutation UpdateTarifa(
    $input: UpdateTarifaInput!
    $condition: ModelTarifaConditionInput
  ) {
    updateTarifa(input: $input, condition: $condition) {
      id
      description
      type
      status
      value
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteTarifa = /* GraphQL */ `
  mutation DeleteTarifa(
    $input: DeleteTarifaInput!
    $condition: ModelTarifaConditionInput
  ) {
    deleteTarifa(input: $input, condition: $condition) {
      id
      description
      type
      status
      value
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createHabitacion = /* GraphQL */ `
  mutation CreateHabitacion(
    $input: CreateHabitacionInput!
    $condition: ModelHabitacionConditionInput
  ) {
    createHabitacion(input: $input, condition: $condition) {
      id
      number
      type
      description
      status
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      ctaHabitacionID
      ctaHabitacion {
        id
        habitacionID
        habitacion {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        date
        cargos
        abonos
        saldos
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateHabitacion = /* GraphQL */ `
  mutation UpdateHabitacion(
    $input: UpdateHabitacionInput!
    $condition: ModelHabitacionConditionInput
  ) {
    updateHabitacion(input: $input, condition: $condition) {
      id
      number
      type
      description
      status
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      ctaHabitacionID
      ctaHabitacion {
        id
        habitacionID
        habitacion {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        date
        cargos
        abonos
        saldos
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteHabitacion = /* GraphQL */ `
  mutation DeleteHabitacion(
    $input: DeleteHabitacionInput!
    $condition: ModelHabitacionConditionInput
  ) {
    deleteHabitacion(input: $input, condition: $condition) {
      id
      number
      type
      description
      status
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      ctaHabitacionID
      ctaHabitacion {
        id
        habitacionID
        habitacion {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        date
        cargos
        abonos
        saldos
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createHuesped = /* GraphQL */ `
  mutation CreateHuesped(
    $input: CreateHuespedInput!
    $condition: ModelHuespedConditionInput
  ) {
    createHuesped(input: $input, condition: $condition) {
      id
      docType
      docNumber
      docImg
      type
      givenName
      lastName
      address
      birthdate
      nationality
      gender
      phone
      cellphone
      email
      contact
      comments
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateHuesped = /* GraphQL */ `
  mutation UpdateHuesped(
    $input: UpdateHuespedInput!
    $condition: ModelHuespedConditionInput
  ) {
    updateHuesped(input: $input, condition: $condition) {
      id
      docType
      docNumber
      docImg
      type
      givenName
      lastName
      address
      birthdate
      nationality
      gender
      phone
      cellphone
      email
      contact
      comments
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteHuesped = /* GraphQL */ `
  mutation DeleteHuesped(
    $input: DeleteHuespedInput!
    $condition: ModelHuespedConditionInput
  ) {
    deleteHuesped(input: $input, condition: $condition) {
      id
      docType
      docNumber
      docImg
      type
      givenName
      lastName
      address
      birthdate
      nationality
      gender
      phone
      cellphone
      email
      contact
      comments
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createCliente = /* GraphQL */ `
  mutation CreateCliente(
    $input: CreateClienteInput!
    $condition: ModelClienteConditionInput
  ) {
    createCliente(input: $input, condition: $condition) {
      id
      docType
      docNumber
      type
      givenName
      lastName
      businessName
      address
      phone
      cellphone
      email
      contact
      comments
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateCliente = /* GraphQL */ `
  mutation UpdateCliente(
    $input: UpdateClienteInput!
    $condition: ModelClienteConditionInput
  ) {
    updateCliente(input: $input, condition: $condition) {
      id
      docType
      docNumber
      type
      givenName
      lastName
      businessName
      address
      phone
      cellphone
      email
      contact
      comments
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteCliente = /* GraphQL */ `
  mutation DeleteCliente(
    $input: DeleteClienteInput!
    $condition: ModelClienteConditionInput
  ) {
    deleteCliente(input: $input, condition: $condition) {
      id
      docType
      docNumber
      type
      givenName
      lastName
      businessName
      address
      phone
      cellphone
      email
      contact
      comments
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createReserva = /* GraphQL */ `
  mutation CreateReserva(
    $input: CreateReservaInput!
    $condition: ModelReservaConditionInput
  ) {
    createReserva(input: $input, condition: $condition) {
      id
      clienteID
      clienteMail
      cliente {
        id
        docType
        docNumber
        type
        givenName
        lastName
        businessName
        address
        phone
        cellphone
        email
        contact
        comments
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      huespedes {
        items {
          id
          docType
          docNumber
          docImg
          type
          givenName
          lastName
          address
          birthdate
          nationality
          gender
          phone
          cellphone
          email
          contact
          comments
          reservaID
          createdAt
          updatedAt
        }
        nextToken
      }
      nightQty
      roomQty
      habitaciones {
        items {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        nextToken
      }
      dateIn
      dateOut
      paymentMethod
      pensionType
      status
      adultPAX
      kidPAX
      babyPAX
      totalPAX
      PAXinside
      tarifaID
      tarifa {
        id
        description
        type
        status
        value
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      tarifaTotal
      comments
      createdBy
      updatedBy
      createdAt
      updatedAt
    }
  }
`;
export const updateReserva = /* GraphQL */ `
  mutation UpdateReserva(
    $input: UpdateReservaInput!
    $condition: ModelReservaConditionInput
  ) {
    updateReserva(input: $input, condition: $condition) {
      id
      clienteID
      clienteMail
      cliente {
        id
        docType
        docNumber
        type
        givenName
        lastName
        businessName
        address
        phone
        cellphone
        email
        contact
        comments
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      huespedes {
        items {
          id
          docType
          docNumber
          docImg
          type
          givenName
          lastName
          address
          birthdate
          nationality
          gender
          phone
          cellphone
          email
          contact
          comments
          reservaID
          createdAt
          updatedAt
        }
        nextToken
      }
      nightQty
      roomQty
      habitaciones {
        items {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        nextToken
      }
      dateIn
      dateOut
      paymentMethod
      pensionType
      status
      adultPAX
      kidPAX
      babyPAX
      totalPAX
      PAXinside
      tarifaID
      tarifa {
        id
        description
        type
        status
        value
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      tarifaTotal
      comments
      createdBy
      updatedBy
      createdAt
      updatedAt
    }
  }
`;
export const deleteReserva = /* GraphQL */ `
  mutation DeleteReserva(
    $input: DeleteReservaInput!
    $condition: ModelReservaConditionInput
  ) {
    deleteReserva(input: $input, condition: $condition) {
      id
      clienteID
      clienteMail
      cliente {
        id
        docType
        docNumber
        type
        givenName
        lastName
        businessName
        address
        phone
        cellphone
        email
        contact
        comments
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      huespedes {
        items {
          id
          docType
          docNumber
          docImg
          type
          givenName
          lastName
          address
          birthdate
          nationality
          gender
          phone
          cellphone
          email
          contact
          comments
          reservaID
          createdAt
          updatedAt
        }
        nextToken
      }
      nightQty
      roomQty
      habitaciones {
        items {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        nextToken
      }
      dateIn
      dateOut
      paymentMethod
      pensionType
      status
      adultPAX
      kidPAX
      babyPAX
      totalPAX
      PAXinside
      tarifaID
      tarifa {
        id
        description
        type
        status
        value
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      tarifaTotal
      comments
      createdBy
      updatedBy
      createdAt
      updatedAt
    }
  }
`;
export const createCuentaHabitacion = /* GraphQL */ `
  mutation CreateCuentaHabitacion(
    $input: CreateCuentaHabitacionInput!
    $condition: ModelCuentaHabitacionConditionInput
  ) {
    createCuentaHabitacion(input: $input, condition: $condition) {
      id
      habitacionID
      habitacion {
        id
        number
        type
        description
        status
        reservaID
        reserva {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        ctaHabitacionID
        ctaHabitacion {
          id
          habitacionID
          date
          cargos
          abonos
          saldos
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      date
      cargos
      abonos
      saldos
      createdAt
      updatedAt
    }
  }
`;
export const updateCuentaHabitacion = /* GraphQL */ `
  mutation UpdateCuentaHabitacion(
    $input: UpdateCuentaHabitacionInput!
    $condition: ModelCuentaHabitacionConditionInput
  ) {
    updateCuentaHabitacion(input: $input, condition: $condition) {
      id
      habitacionID
      habitacion {
        id
        number
        type
        description
        status
        reservaID
        reserva {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        ctaHabitacionID
        ctaHabitacion {
          id
          habitacionID
          date
          cargos
          abonos
          saldos
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      date
      cargos
      abonos
      saldos
      createdAt
      updatedAt
    }
  }
`;
export const deleteCuentaHabitacion = /* GraphQL */ `
  mutation DeleteCuentaHabitacion(
    $input: DeleteCuentaHabitacionInput!
    $condition: ModelCuentaHabitacionConditionInput
  ) {
    deleteCuentaHabitacion(input: $input, condition: $condition) {
      id
      habitacionID
      habitacion {
        id
        number
        type
        description
        status
        reservaID
        reserva {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        ctaHabitacionID
        ctaHabitacion {
          id
          habitacionID
          date
          cargos
          abonos
          saldos
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      date
      cargos
      abonos
      saldos
      createdAt
      updatedAt
    }
  }
`;
export const createUsuario = /* GraphQL */ `
  mutation CreateUsuario(
    $input: CreateUsuarioInput!
    $condition: ModelUsuarioConditionInput
  ) {
    createUsuario(input: $input, condition: $condition) {
      id
      email
      type
      createdAt
      updatedAt
    }
  }
`;
export const updateUsuario = /* GraphQL */ `
  mutation UpdateUsuario(
    $input: UpdateUsuarioInput!
    $condition: ModelUsuarioConditionInput
  ) {
    updateUsuario(input: $input, condition: $condition) {
      id
      email
      type
      createdAt
      updatedAt
    }
  }
`;
export const deleteUsuario = /* GraphQL */ `
  mutation DeleteUsuario(
    $input: DeleteUsuarioInput!
    $condition: ModelUsuarioConditionInput
  ) {
    deleteUsuario(input: $input, condition: $condition) {
      id
      email
      type
      createdAt
      updatedAt
    }
  }
`;
