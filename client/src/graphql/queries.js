/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getTarifa = /* GraphQL */ `
  query GetTarifa($id: ID!) {
    getTarifa(id: $id) {
      id
      description
      type
      status
      value
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listTarifas = /* GraphQL */ `
  query ListTarifas(
    $filter: ModelTarifaFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTarifas(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        description
        type
        status
        value
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getHabitacion = /* GraphQL */ `
  query GetHabitacion($id: ID!) {
    getHabitacion(id: $id) {
      id
      number
      type
      description
      status
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      ctaHabitacionID
      ctaHabitacion {
        id
        habitacionID
        habitacion {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        date
        cargos
        abonos
        saldos
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listHabitacions = /* GraphQL */ `
  query ListHabitacions(
    $filter: ModelHabitacionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listHabitacions(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        number
        type
        description
        status
        reservaID
        reserva {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        ctaHabitacionID
        ctaHabitacion {
          id
          habitacionID
          date
          cargos
          abonos
          saldos
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getHuesped = /* GraphQL */ `
  query GetHuesped($id: ID!) {
    getHuesped(id: $id) {
      id
      docType
      docNumber
      docImg
      type
      givenName
      lastName
      address
      birthdate
      nationality
      gender
      phone
      cellphone
      email
      contact
      comments
      reservaID
      reserva {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listHuespeds = /* GraphQL */ `
  query ListHuespeds(
    $filter: ModelHuespedFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listHuespeds(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        docType
        docNumber
        docImg
        type
        givenName
        lastName
        address
        birthdate
        nationality
        gender
        phone
        cellphone
        email
        contact
        comments
        reservaID
        reserva {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getCliente = /* GraphQL */ `
  query GetCliente($id: ID!) {
    getCliente(id: $id) {
      id
      docType
      docNumber
      type
      givenName
      lastName
      businessName
      address
      phone
      cellphone
      email
      contact
      comments
      reservas {
        items {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listClientes = /* GraphQL */ `
  query ListClientes(
    $filter: ModelClienteFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listClientes(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        docType
        docNumber
        type
        givenName
        lastName
        businessName
        address
        phone
        cellphone
        email
        contact
        comments
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getReserva = /* GraphQL */ `
  query GetReserva($id: ID!) {
    getReserva(id: $id) {
      id
      clienteID
      clienteMail
      cliente {
        id
        docType
        docNumber
        type
        givenName
        lastName
        businessName
        address
        phone
        cellphone
        email
        contact
        comments
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      huespedes {
        items {
          id
          docType
          docNumber
          docImg
          type
          givenName
          lastName
          address
          birthdate
          nationality
          gender
          phone
          cellphone
          email
          contact
          comments
          reservaID
          createdAt
          updatedAt
        }
        nextToken
      }
      nightQty
      roomQty
      habitaciones {
        items {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        nextToken
      }
      dateIn
      dateOut
      paymentMethod
      pensionType
      status
      adultPAX
      kidPAX
      babyPAX
      totalPAX
      PAXinside
      tarifaID
      tarifa {
        id
        description
        type
        status
        value
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      tarifaTotal
      comments
      createdBy
      updatedBy
      createdAt
      updatedAt
    }
  }
`;
export const listReservas = /* GraphQL */ `
  query ListReservas(
    $filter: ModelReservaFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listReservas(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getCuentaHabitacion = /* GraphQL */ `
  query GetCuentaHabitacion($id: ID!) {
    getCuentaHabitacion(id: $id) {
      id
      habitacionID
      habitacion {
        id
        number
        type
        description
        status
        reservaID
        reserva {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        ctaHabitacionID
        ctaHabitacion {
          id
          habitacionID
          date
          cargos
          abonos
          saldos
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      date
      cargos
      abonos
      saldos
      createdAt
      updatedAt
    }
  }
`;
export const listCuentaHabitacions = /* GraphQL */ `
  query ListCuentaHabitacions(
    $filter: ModelCuentaHabitacionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listCuentaHabitacions(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        habitacionID
        habitacion {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        date
        cargos
        abonos
        saldos
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getUsuario = /* GraphQL */ `
  query GetUsuario($id: ID!) {
    getUsuario(id: $id) {
      id
      email
      type
      createdAt
      updatedAt
    }
  }
`;
export const listUsuarios = /* GraphQL */ `
  query ListUsuarios(
    $filter: ModelUsuarioFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsuarios(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        email
        type
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const tarifaByType = /* GraphQL */ `
  query TarifaByType(
    $type: String
    $sortDirection: ModelSortDirection
    $filter: ModelTarifaFilterInput
    $limit: Int
    $nextToken: String
  ) {
    tarifaByType(
      type: $type
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        description
        type
        status
        value
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const habitacionByNumber = /* GraphQL */ `
  query HabitacionByNumber(
    $number: String
    $sortDirection: ModelSortDirection
    $filter: ModelHabitacionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    habitacionByNumber(
      number: $number
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        number
        type
        description
        status
        reservaID
        reserva {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        ctaHabitacionID
        ctaHabitacion {
          id
          habitacionID
          date
          cargos
          abonos
          saldos
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const habitacionByReserva = /* GraphQL */ `
  query HabitacionByReserva(
    $reservaID: ID
    $sortDirection: ModelSortDirection
    $filter: ModelHabitacionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    habitacionByReserva(
      reservaID: $reservaID
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        number
        type
        description
        status
        reservaID
        reserva {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        ctaHabitacionID
        ctaHabitacion {
          id
          habitacionID
          date
          cargos
          abonos
          saldos
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const huespedByDocNumber = /* GraphQL */ `
  query HuespedByDocNumber(
    $docNumber: String
    $sortDirection: ModelSortDirection
    $filter: ModelHuespedFilterInput
    $limit: Int
    $nextToken: String
  ) {
    huespedByDocNumber(
      docNumber: $docNumber
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        docType
        docNumber
        docImg
        type
        givenName
        lastName
        address
        birthdate
        nationality
        gender
        phone
        cellphone
        email
        contact
        comments
        reservaID
        reserva {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const huespedByReserva = /* GraphQL */ `
  query HuespedByReserva(
    $reservaID: ID
    $sortDirection: ModelSortDirection
    $filter: ModelHuespedFilterInput
    $limit: Int
    $nextToken: String
  ) {
    huespedByReserva(
      reservaID: $reservaID
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        docType
        docNumber
        docImg
        type
        givenName
        lastName
        address
        birthdate
        nationality
        gender
        phone
        cellphone
        email
        contact
        comments
        reservaID
        reserva {
          id
          clienteID
          clienteMail
          nightQty
          roomQty
          dateIn
          dateOut
          paymentMethod
          pensionType
          status
          adultPAX
          kidPAX
          babyPAX
          totalPAX
          PAXinside
          tarifaID
          tarifaTotal
          comments
          createdBy
          updatedBy
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const clienteByDocNumber = /* GraphQL */ `
  query ClienteByDocNumber(
    $docNumber: String
    $sortDirection: ModelSortDirection
    $filter: ModelClienteFilterInput
    $limit: Int
    $nextToken: String
  ) {
    clienteByDocNumber(
      docNumber: $docNumber
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        docType
        docNumber
        type
        givenName
        lastName
        businessName
        address
        phone
        cellphone
        email
        contact
        comments
        reservas {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const reservaByCliente = /* GraphQL */ `
  query ReservaByCliente(
    $clienteID: ID
    $sortDirection: ModelSortDirection
    $filter: ModelReservaFilterInput
    $limit: Int
    $nextToken: String
  ) {
    reservaByCliente(
      clienteID: $clienteID
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const reservaByID = /* GraphQL */ `
  query ReservaByID(
    $id: ID
    $sortDirection: ModelSortDirection
    $filter: ModelReservaFilterInput
    $limit: Int
    $nextToken: String
  ) {
    reservaByID(
      id: $id
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        clienteID
        clienteMail
        cliente {
          id
          docType
          docNumber
          type
          givenName
          lastName
          businessName
          address
          phone
          cellphone
          email
          contact
          comments
          createdAt
          updatedAt
        }
        huespedes {
          nextToken
        }
        nightQty
        roomQty
        habitaciones {
          nextToken
        }
        dateIn
        dateOut
        paymentMethod
        pensionType
        status
        adultPAX
        kidPAX
        babyPAX
        totalPAX
        PAXinside
        tarifaID
        tarifa {
          id
          description
          type
          status
          value
          createdAt
          updatedAt
        }
        tarifaTotal
        comments
        createdBy
        updatedBy
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const cuentaByHabitacion = /* GraphQL */ `
  query CuentaByHabitacion(
    $habitacionID: ID
    $sortDirection: ModelSortDirection
    $filter: ModelCuentaHabitacionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    cuentaByHabitacion(
      habitacionID: $habitacionID
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        habitacionID
        habitacion {
          id
          number
          type
          description
          status
          reservaID
          ctaHabitacionID
          createdAt
          updatedAt
        }
        date
        cargos
        abonos
        saldos
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
